import ROOT
import pandas as pd
import numpy as np
import argparse
import sys
import os

def prepareCalibrationDataFrame(files,root='ana'):
    """
    wraps the procedure of selecting the events and collecting the necessary info in a small dataframe
    returns a pandas DataFrame.
    files - a list of files
    sum_defs - the sum definitions (see previous cell for the definition of the dict)
    """
                
    ROOT.ROOT.EnableImplicitMT()
        
    #define the selections and branches with an RDataFrame
    rdf = ROOT.RDataFrame(f"{root}/hits", files)
    start_count=rdf.Count()
    rdf = rdf.Filter('crosscalo>0')
    filter_count=rdf.Count()
    
    print('\tTotal at start:',start_count.GetValue())
    print('\tTotal pre-selected:',filter_count.GetValue())
    
    #filter only the columns of interest
    df = pd.DataFrame(rdf.AsNumpy())
    df = df.astype({'crosscalo':'bool'}) #booleans are propagated as objects in pandas
    
    col_map = [ ('e_cee120','sumen_CEESi100'),('e_cee200','sumen_CEESi200'),('e_cee300','sumen_CEESi300') ]
    col_map += [ 
('e_cehfine120','sumen_CEHfineSi100'),('e_cehfine200','sumen_CEHfineSi200'),('e_cehfine300','sumen_CEHfineSi300') ]
    col_map += [ 
('e_cehcoarse120','sumen_CEHcoarseSi100'),('e_cehcoarse200','sumen_CEHcoarseSi200'),('e_cehcoarse300','sumen_CEHcoarseSi300') 
]
    col_map += [ ('e_cehscifine','sumen_CEHScifine'),('e_cehscicoarse','sumen_CEHScicoarse') ]
    col_map += [ ('genergy','genEn'), ('gpt','genEt'), ('geta','genEta'),('gphi','genPhi'),('c_glob','c_glob')]
    col_map = dict(col_map)
    df.rename(inplace=True,columns=col_map)
    df.drop(columns=['gvradius','gvz'],inplace=True)
    
    ROOT.ROOT.DisableImplicitMT()
    
    return df

def main(raw_args=None):

    """
    takes care of calling the prepareCalibrationDataFrame method 
    for each analysis sub-directory in the ROOT files of interest
    """
    parser = argparse.ArgumentParser( prog='prepareSummaries',
                                      description='converts the ROOT files created with CMSSW to final dataframes for the studies',
                                      epilog='The results are to be used with the notebooks')
    parser.add_argument('-i', '--input', help='csv list with files to use')
    parser.add_argument('-o', '--output', help='output directory')
    parser.add_argument('-f', '--force', help='force overwrite', action='store_true')
    args = parser.parse_args(raw_args)
    
    #prepare output
    os.system(f'mkdir -p {args.output}')

    #create file list
    flist=args.input.split(',')
    print(flist)
    print(f'Processing {len(flist)} files with output @ {args.output}')

    #read sub-directories from ROOT file
    fIn=ROOT.TFile.Open(flist[0])
    rootlist=[d.GetName() for d in fIn.GetListOfKeys()]
    fIn.Close()
    
    #process all the sub-directories
    for root in rootlist:

        try:

            out_f=os.path.join(args.output,'energysums_{}.feather'.format(root))
            if os.path.isfile(out_f) and not args.force: continue

            df=prepareCalibrationDataFrame(flist,root=root)

            #filter pseudo-rapidity
            df['genAbsEta'] = np.abs(df['genEta'])
            df=df[ (df['genAbsEta']>=1.5) & (df['genAbsEta']<=3.0) & (df['genEn']>10) & (df['genEn']<500) ]

            #categorize
            df['eta_cat'] = np.digitize(df['genAbsEta'], bins=[1.7,1.9,2.1,2.6,3.0])
            df['en_cat'] = np.digitize(df['genEn'], bins=[10,15,20,25,30,40,50,60,70,80,100,120,150,200,250,300,350,400,450,500])
    
            df.reset_index(inplace=True)
            df=df.drop(columns=['index'])
            df.to_feather(out_f)
    
            print('\t{} created with shape={}'.format(out_f,df.shape))
            del df
            
        except Exception as e:
            print(f'\t error while processing {root}',e)
            

if __name__ == '__main__':

    raw_args=sys.argv[1:]
    if len(raw_args)==0:
        indir='/eos/cms/store/group/dpg_hgcal/comm_hgcal/psilva/K0L_studies/2025Jan30_lc1/'
        fcsv=','.join(
            [os.path.join(indir,f) for f in os.listdir(indir) 
             if '.root' in f and 'SingleK0LGun' in f and not '1p6to2p9' in f]
        )
        outdir=f'{indir}/summaries/K0LFixedEta'
        raw_args=['-i', fcsv, '-o', outdir]

    sys.exit(main(raw_args))
