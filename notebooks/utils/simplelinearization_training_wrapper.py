#!/usr/bin/env python
# coding: utf-8

# # Simple energy linearization train wrapper
# 
# This notebook wraps the procedure of training the energy linearization model 
# 
# Author: P. Silva (psilva@cern.ch)
# 
# Date: 23/03/2022

import os
import sys
import datetime
import pandas as pd
import torch
import numpy as np
import argparse
from energy_regression_tools import *
from tqdm import tqdm
import mplhep as hep
plt.style.use([hep.style.CMS, hep.style.firamath])

def prepareForCalib(df,maxfcee):
    
    """prepares a dataframe for calibration"""

    x_features=[x for x in df.columns if 'sumen_' in x]
    df['en_l0']=df[x_features].sum(axis=1)
    df['fcee']=df[['sumen_CEESi100','sumen_CEESi200','sumen_CEESi300',]].sum(axis=1)/df['en_l0']
    
    if not maxfcee is None: 
        mask=(df['fcee']<maxfcee)
        print(f'Applying mask for maxfcee<{maxfcee} will select {mask.sum()} out of {df.shape[0]} showers')
        df=df[mask]
    
    return df,x_features


def main(args):
    
    parser = argparse.ArgumentParser(description='usage: %prog [options]')
    parser.add_argument('-i', '--input', default=None, help='input directory (or file) to process')
    parser.add_argument('-o', '--output', default=None, help='output directory (if None save under training)')
    parser.add_argument('--maxFCEE', default=None, help='maximum fraction of raw energy in CE-E', type=float)
    parser.add_argument('-f', '--force', help='force overwrite', action='store_true')
    opt=parser.parse_args(args)

    flist=[opt.input]
    if os.path.isdir(opt.input):
        flist=[os.path.join(opt.input,f) for f in os.listdir(opt.input) if '.feather' in f]

    outdir=opt.output
    if outdir is None:
        basedir=os.path.dirname(flist[0])
        outdir=os.path.join(basedir,'training')
    plotdir=os.path.join(outdir,'plots')
    os.system(f'mkdir -p {plotdir}')

    tag='' if opt.maxFCEE is None else f'_maxfcee{opt.maxFCEE:3.2f}'
    tag=tag.replace('.','p')

    #loop over the files to calibrate
    for url in tqdm(flist):

        calib_tag=os.path.splitext(os.path.basename(url))[0]
        calib_tag += tag

        coeffs_url=f'{outdir}/l1coeffs_{calib_tag}.feather'
        regcoeffs_url=coeffs_url.replace('l1coeffs','l1regcoeffs')

        if os.path.isfile(coeffs_url) and os.path.isfile(regcoeffs_url) and not opt.force:
            continue
        
        print(f'Setting energy scale for {calib_tag}')

        #read dataset
        print(url)
        df=pd.read_feather(url)
        df,x_features=prepareForCalib(df,opt.maxFCEE)
        
        #run the regression for the relative weights of the different sections
        coeffs=doL1Regression(df,x_features,outdir=None)
        coeffs.to_feather(coeffs_url)
        print('\tCalibration coefficients have been stored in',coeffs_url)
    
        #regularize the coefficients with a functional form
        reg_coeffs=regularizeCalibCoeffs(coeffs,outdir=plotdir,plot_tag=calib_tag)        
        reg_coeffs.to_feather(regcoeffs_url)
        print('\tRegularized calibration coefficients have been stored in',regcoeffs_url)
    
                
if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
