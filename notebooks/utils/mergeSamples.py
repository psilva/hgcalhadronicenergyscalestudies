import re
import sys
import os

def main():
    
    patt='(.*)_(\d+).root'
    
    url=sys.argv[1]

    # build list of samples
    samples_dict={}
    for f in os.listdir(url):
        if not '.root' in f : continue
        tag,idx=re.findall(patt,f)[0]
        if not tag in samples_dict: 
            samples_dict[tag]=[]
        samples_dict[tag].append( os.path.join(url,f) )

    #hadd the samples
    os.system(f'mkdir -p {url}/Chunks')
    for tag,fl in samples_dict.items():
        print(f'hadding {tag} with {len(fl)} chunks')
        flstr=' '.join(fl)
        os.system(f'hadd -f -k {tag}.root {flstr}')
        for f in fl:
            os.system(f'mv {f} {url}/Chunks/')
        os.system(f'mv {tag}.root {url}/')

if __name__ == '__main__':
    main()
