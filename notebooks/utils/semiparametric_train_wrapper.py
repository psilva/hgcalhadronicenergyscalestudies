#!/usr/bin/env python
# coding: utf-8

# # Semi-parameteric energy regression model
# 
# This notebook wraps the procedure of training the semi-parameteric model
# 
# Author: P. Silva (psilva@cern.ch)
# 
# Date: 23/03/2022

import os
import sys
import datetime
import pandas as pd
import torch
import numpy as np
import argparse
import semiparametric_model as SPModel
from tqdm import tqdm

def prepareForTrain(df,addglobalfeat=['fcee','c_glob']):

    """prepares the dataframe for training applying a pre-selection and selecting the appropriate columns"""
        
    #initial estimate of the energy and fraction of energies in CE-E
    x_local_features=[x for x in df.columns if 'sumen_' in x]
    df['en_l0']=df[x_local_features].sum(axis=1) 
    df['fcee']=np.where(df['en_l0']>0,
                        df[['sumen_CEESi100','sumen_CEESi200','sumen_CEESi300',]].sum(axis=1)/df['en_l0'],
                        0.)
           
    #global features
    x_global_features=['en_l0','genAbsEta','genPhi']
    x_global_features += [x for x in addglobalfeat if x in df.columns]
    
    #response (target)
    df['r']=df['genEn']/df['en_l0']
   
    #run the regression only for reasonable values of the response
    mask = (df['en_l0']>0) & (df['r']>0) & (df['r']<4)
    df = df[mask]
    
    return df,x_global_features,x_local_features


def getTrainingInputs(url,train_tag,trainfrac=1.0,addglobalfeat=['fcee','c_glob']):

    """prepares the features and target tensors for the training from a dataset"""

    tag=datetime.datetime.now().strftime(f'_{train_tag}_%Y%b%d')

    df=pd.read_feather(url)
    df,globalfeat,localfeat=prepareForTrain(df,addglobalfeat=addglobalfeat)
    print(f'Training will be based on global features={globalfeat}')
    print(f'Local features will be weighted parameterically features={localfeat}')

    #tensor of features
    X=[df[col].values for col in globalfeat+localfeat]
    X=np.stack(X, 1)
    X=torch.tensor(X, dtype=torch.float)

    #vector of targets
    y=torch.tensor(df['r'].values, dtype=torch.float).reshape(-1,1)

    #a random shuffle is applied and only a fraction is used for train/test
    ntot=X.shape[0]
    nfortrain=int(trainfrac*ntot)
    print(f'Using {nfortrain} out of {ntot}')

    rnd_idx = torch.randperm(ntot)
    X=X[rnd_idx][:nfortrain]
    y=y[rnd_idx][:nfortrain]
    
    del df
    
    return X,y,tag,globalfeat,localfeat


def param_func_torch(x, a, b, c, d):
    
    """function used to regularize energy scale correction"""

    return a+b/torch.sqrt(x+1)+c/(x+1)+d*x


def runTraining(indir,outdir,summary,addglobalfeat,dosigma,trainkey):
          
    url=f'{indir}/{summary}'
        
    #get tensors for training
    X,y,tag,globalfeat,localfeat = getTrainingInputs(url,trainkey,addglobalfeat=addglobalfeat)

    #training takes almost 24h on a CPU a couple of minutes on a GPU
    layers=[1024,512,256,128]
    if not dosigma: layers=[512,256,128]
    modelurl,logurl = SPModel.train_validate_semiparametric(X=X,y=y,nparamfeat=len(localfeat),
                                                            npopt=4,callback=param_func_torch,
                                                            epochs=200,lr=0.001,
                                                            layers=layers,
                                                            root=outdir,
                                                            tag=tag,
                                                            seed=41,
                                                            dosigma=dosigma)
    
    return modelurl, logurl

    
def set_totStudies_lc1():
    
    """trainings needed for the clustering / TOT studies"""
  
    #train_sets={
    #    'lc_ca_r04'                   : ['energysums_ana_lc_ca_r04.feather',                   ['fcee','c_glob'], False],
    #    'lc_ca_r02'                   : ['energysums_ana_lc_ca_r02.feather',                   ['fcee','c_glob'], False],
    #    'lc_ca_r070_si'               : ['energysums_ana_lc_ca_r070_si.feather',               ['fcee','c_glob'], False],
    #    'lc_ca_r070_si_pca'           : ['energysums_ana_lc_ca_r070_si_pca.feather',           ['fcee','c_glob'], False],
    #    'tk_pca'                      : ['energysums_ana_tk_pca.feather',                      ['fcee','c_glob'], False],
    #    'lc_ca_r04_notot'             : ['energysums_ana_lc_ca_r04_notot.feather',             ['fcee','c_glob'], False],
    #    'lc_ca_r04_nototlast'         : ['energysums_ana_lc_ca_r04_nototlast.feather',         ['fcee','c_glob'], False],
    #    'lc_ca_r02_notot'             : ['energysums_ana_lc_ca_r02_notot.feather',             ['fcee','c_glob'], False],
    #    'lc_ca_r02_nototlast'         : ['energysums_ana_lc_ca_r02_nototlast.feather',         ['fcee','c_glob'], False],
    #    'lc_ca_r070_si_notot'         : ['energysums_ana_lc_ca_r070_si_notot.feather',         ['fcee','c_glob'], False],
    #    'lc_ca_r070_si_nototlast'     : ['energysums_ana_lc_ca_r070_si_nototlast.feather',     ['fcee','c_glob'], False],
    #    'lc_ca_r070_si_pca_notot'     : ['energysums_ana_lc_ca_r070_si_pca_notot.feather',     ['fcee','c_glob'], False],
    #    'lc_ca_r070_si_pca_nototlast' : ['energysums_ana_lc_ca_r070_si_pca_nototlast.feather', ['fcee','c_glob'], False],
    #    'tk_pca_notot'                : ['energysums_ana_tk_pca_notot.feather',                ['fcee','c_glob'], False],
    #    'tk_pca_nototlast'            : ['energysums_ana_tk_pca_nototlast.feather',            ['fcee','c_glob'], False],
    #}
    #indir='/eos/cms/store/group/dpg_hgcal/comm_hgcal/psilva/K0L_studies/2024Apr27_lc1/summaries/K0LScanEta/'

    analysis_sets = [ "ana_lc_ca_r04", "ana_lc_ca_r04_1", "ana_lc_ca_r04_2", "ana_lc_ca_r04_3", "ana_lc_ca_r04_4", "ana_lc_ca_r04_5", "ana_lc_ca_r04_6", "ana_lc_ca_r02", "ana_lc_ca_r070_si", "ana_lc_ca_r070_si_pca", "ana_lc_ca_r070_si_pca_1", "ana_lc_ca_r070_si_pca_2", "ana_lc_ca_r070_si_pca_3", "ana_lc_ca_r070_si_pca_4", "ana_lc_ca_r070_si_pca_5", "ana_lc_ca_r070_si_pca_6", "ana_tk", "ana_tk_pca", "ana_tk_pca_1", "ana_tk_pca_2", "ana_tk_pca_3","ana_tk_pca_4", "ana_tk_pca_5", "ana_tk_pca_6"]
    train_sets = dict( [ (a, [f'energysums_{a}.feather', ['fcee','c_glob'], False]) for a in analysis_sets ] )
    indir='/eos/cms/store/group/dpg_hgcal/comm_hgcal/psilva/K0L_studies/2025Jan30_lc1/summaries/K0LScanEta/'
    
    outdir=f'{indir}/training'

    return indir,outdir,train_sets


def set_totStudies_lc2():

    """similar for clustering / TOT studies with min LC hits of 2"""

    indir,outdir,train_sets = set_totStudies_lc1()
    indir = indir.replace('lc1','lc2')
    outdir = outdir.replace('lc1','lc2')
    return indir,outdir,train_sets
        

def main(args):
    
    parser = argparse.ArgumentParser(description='usage: %prog [options]')
    parser.add_argument('-s', '--set', default='totStudies_lc1',
                        help='set to use in the training (default: %(default)s)')
    parser.add_argument('--dryRun', help='do not run anything', action='store_true')
    opt=parser.parse_args(args)

    #call function that defines the training set
    indir,outdir,train_sets = globals()[f'set_{opt.set}']()
    print(f'Input directory: {indir}')
    print(f'Output directory: {outdir}')
    print(f'Will launch {len(train_sets)} trainings')
    if torch.cuda.is_available():
        print(f'CUDA is available device: {torch.cuda.get_device_name(0)}')
    else:
        print(f'With CPU only this is going to take a looooong time...')

    #resign if it's a dry run
    if opt.dryRun: return 0

    os.system(f'mkdir -p {outdir}')
    
    #run trainings
    for key, val in tqdm(train_sets.items()):
        summary,addglobalfeat,dosigma = val
        modelurl,logurl = runTraining(indir,outdir,summary,addglobalfeat,dosigma,key)
                
if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
