#!/usr/bin/env python
# coding: utf-8

# # Semi-parameteric energy regression model
# 
# This notebook wraps the procedure of running the semi-parameteric model for inference
# 
# Author: P. Silva (psilva@cern.ch)
# 
# Date: 23/03/2022

import os
import sys
import datetime
import pandas as pd
import torch
import numpy as np
import semiparametric_model as SPModel
from semiparametric_train_wrapper import prepareForTrain, param_func_torch
from simplelinearization_predict_wrapper import submitRunPrediction
from energy_regression_tools import doResiduals
import matplotlib.pyplot as plt
import mplhep as hep
plt.style.use([hep.style.CMS, hep.style.firamath])

def runSemiparametricPrediction(inurl,modelurl,outurl,maxfcee,plotdir,plottag,**kwargs):

    """runs the prediction over a dataset"""
    
    addglobalfeat=kwargs['addglobalfeat']

    #prepare the features for the inference
    df=pd.read_feather(inurl)
    df,globalfeat,localfeat=prepareForTrain(df,addglobalfeat=addglobalfeat)
    X=[df[col].values for col in globalfeat+localfeat]
    X=np.stack(X, 1)
    X=torch.tensor(X, dtype=torch.float)    

    #load model
    model = torch.load(modelurl)
    if torch.cuda.is_available():
        X=X.cuda()
        model=model.cuda()
    
    #run the inference
    R, sigma, param_feats = SPModel.predict_semiparametric(X,model,globalfeat+localfeat,full=True)
    en_l0=df['en_l0']
    df['en_l1'] = R*en_l0
    df['en_l1_sigma'] = sigma*en_l0
    
    #add a column with the corrected value of the local feature
    if not param_feats is None:
        for ifeat,feat in enumerate(localfeat):
            delta=param_feats[:,ifeat]
            df[feat+'_reg']=df[feat]+delta
    
    #compute the residuals
    plot_tag=os.path.basename(outurl).replace('.feather','')
    df,_=doResiduals(df,y_pred='en_l1',outdir=plotdir,plot_tag=plot_tag,ylim=(0.6,1.4))

    #save result
    df=df.reset_index()
    df=df.drop(columns=['index'])
    local_outurl=os.path.basename(outurl)
    df.to_feather(local_outurl)
    os.system(f'mv -v {local_outurl} {outurl}')
    
    del model
    del df
    del X

        
def main():

    FIXLC1='/eos/cms/store/group/dpg_hgcal/comm_hgcal/psilva/K0L_studies/2024Apr27_lc1/summaries/K0LFixedEta'
    SCANLC1='/eos/cms/store/group/dpg_hgcal/comm_hgcal/psilva/K0L_studies/2024Apr27_lc1/summaries/K0LEtaScan'
    FIXLC2='/eos/cms/store/group/dpg_hgcal/comm_hgcal/psilva/K0L_studies/2024Apr27_lc2/summaries/K0LFixedEta'
    SCANLC2='/eos/cms/store/group/dpg_hgcal/comm_hgcal/psilva/K0L_studies/2024Apr27_lc2/summaries/K0LEtaScan'
    
    kwargs_dict={'addglobalfeat':['fcee','c_glob']}
    predict_sets=[
        (f'{FIXLC1}/energysums*.feather', f'{SCANLC1}/training/{{}}2024*.torch', None, kwargs_dict),
        (f'{FIXLC2}/energysums*.feather', f'{SCANLC2}/training/{{}}2024*.torch', None, kwargs_dict),
    ]
    
    submitRunPrediction(predict_sets, runSemiparametricPrediction)

if __name__ == '__main__':
    sys.exit(main())
