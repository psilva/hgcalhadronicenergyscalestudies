import time
import torch 
import torch.nn as tnn 
import torch.nn.functional as F
from torch.utils.data import DataLoader,TensorDataset
from torch.optim.lr_scheduler import ExponentialLR
import numpy as np


class SemiParametricModel(tnn.Module):
    
    def __init__(self, nfeat, nparamfeat, npopt, callback, layers, dropout=0.5, activation=tnn.ReLU, dosigma=False):
        """
        This class implements a semi-parametric regression ANN based on pytorch
        It is assumed that we are regressing a ratio of the form 
    
        $y = target / y0$
    
        where target is the final value to be predicted (e.g. an energy)
        The target will be approximated as
    
        $target = y0 + \sum f_i(x) \tilde(x)$
    
        where x=[y0,....] is a vector of global features,  $\tilde{x}$ is a vector of local features, 
        and $f_i(x)$ is a functional form, function of global features.
    
        The network will make use of connected layers to predict the parameters of the functional forms $f_i$
        and will return the ratio 
    
        $[y0 + \sum f_i(x) \tilde(x)] / y0$
    
        as the final result as well as the gaussian width expected on this correction.
        Notice that the width can only be learnt with a gaussian likelihood as the loss function.

        The input variables are the following:
        nfeat - number of global features
        nparamfeat - number of local features
        npopt - number of parameters of the functional form
        callback - callback implementing the functional form
        layers - array of number of neurons in the connected layers
        dropout - dropout fraction
        activation - activation function
        dosigma - if true sigma is parametrized as sqrt(A**2/y0+(B/y0)**2+C**2) where A,B,C will be given by 3 output nodes
                  if false the width tensor will be filled with 0s
        """
        
        #call parent __init__
        super().__init__()
        
        self.nfeat = nfeat-nparamfeat
        self.nparamfeat = nparamfeat
        self.dropout = min(dropout,1)
        self.batchnorm = tnn.BatchNorm1d(self.nfeat)
        self.npopt = npopt
        self.callback = callback
        self.dosigma = dosigma
                
        # define the layer structure
        layerlist = []
        for i,nout in enumerate(layers):
            nin=self.nfeat if i==0 else layers[i-1]
            layerlist.append(tnn.Linear(nin,nout))
            layerlist.append( activation(inplace=True) )
            layerlist.append(tnn.BatchNorm1d(nout))
            if self.dropout>0 : layerlist.append(tnn.Dropout(self.dropout))

        #last layer will produce the required outputs
        if self.dosigma:
            layerlist.append(tnn.Linear(layers[-1],nparamfeat*npopt+3))
        else:
            layerlist.append(tnn.Linear(layers[-1],nparamfeat*npopt))

        #use this to get a positively defined sigma   
        self.softplus = tnn.Softplus()
                    
        #list of layers is converted to a Sequential model as an attribute
        self.layers = tnn.Sequential(*layerlist)
                
    def forward(self,x):
        
        """
        performs the forward pass of the model (calls internal implementation)
        """
        
        return self._forward(x,full=False)
    
    
    def _forward(self, x, full=False):
        
        """
        performs the forward pass of the model
        if full is true, the scaled local features are also return
        """
        
        # extract embedding values from the incoming categorical data and perform dropout
        
        xvar=x[:,0]
        xfeat=x[:,:-self.nparamfeat]
        xparamfeat=x[:,-self.nparamfeat:]
        
        # normalize the incoming continuous data          
        # and evaluate the sequential part of the model
        x = self.batchnorm(xfeat)
        x = self.layers(x)
            
        #now use the predictions for the parameters of the functional form to compute the 
        #final
        delta=[]
        for i in range(self.nparamfeat):
            
            i1=i*self.npopt
            i2=(i+1)*self.npopt
            
            xi=x[:,i1:i2]            
            
            #split the tensor per columns so it can be passed as an array of arrays of parameters for the callback
            # [ [a,b,c,...], [d,e,f,...], ... ] 
            #  -> [ [[a],[d],...], [[b],[e],...], [[c],[f],...], ... ] 
            #  -> [[a,d,...], [b,e,...], [c,f,...], ...]
            xi=torch.split(xi,1,dim=1)
            popt=[ c.flatten() for c in xi ]
            fval=self.callback(xvar,*popt)
            xparamfeati=xparamfeat[:,i]
            delta.append( fval*xparamfeati) 
        
        #compute the regressed result
        delta = torch.stack(delta)
        delta_sum = delta.sum(dim=0)
        xcor=xvar+delta_sum
        
        ones_tensor=torch.ones_like(xvar)
        xout=torch.where(xvar>0,xcor/xvar,ones_tensor)
        
        #compute the associated sigma using the sigma/E = stochastic/sqrt(E) \oplus noise/E \oplus constant model
        if self.dosigma: 
            A=x[:,-3]
            B=x[:,-2]
            C=x[:,-1] 
            sigma=torch.where(xvar>0,
                              torch.sqrt( (A**2)/xvar + (B/xvar)**2 + C**2),
                              ones_tensor)
        else:
            sigma=0*ones_tensor

        #return results
        if full:
            return xout,sigma,delta
        else:
            return xout,sigma




    
def train_validate_semiparametric(X,y,nparamfeat,npopt,callback,
                                  fsplit=0.6,epochs=10,batch_size=1024,lr=1e-3,
                                  layers=[256,128,64,32],dropout=0.5,activation=tnn.ReLU,
                                  seed=41,
                                  dosigma=False,
                                  root='./',
                                  tag='',
                                  max_nnoimprov=10):

    """
    train and validate the semiparametric model
    X,y - features and target
    nparamfeat - number of features for which the paramterization will be derived
    npopt - number of parameters in in the callback function 
    callback - callback function which implements the parameterization form
    fsplit - splitting fraction for training / (1-fsplit) for test
    epochs - number of epochs to train
    batch - batch size
    lr - learning rate
    layers - number of nodes in the layers
    dropout - fraction to dropout
    activation - activation function
    seed - random number generator seed
    dosigma - train for the gaussian width as well
    root - base directory where to store model and log file
    tag - a tag for this training
    """
    
    #use a fixed seed for reproducibility
    torch.manual_seed(seed)
    
    #start the model
    model= SemiParametricModel(nfeat=X.shape[1],nparamfeat=nparamfeat,
                               npopt=npopt,callback=callback,
                               layers=layers,dropout=dropout,activation=activation,
                               dosigma=dosigma)
    
    #if cuda is available turn to CUDA objects
    if torch.cuda.is_available():
        print("Using CUDA as it's available")
        X=X.cuda()
        y=y.cuda()
        model=model.cuda()
    
    modelurl=f'{root}/semiparametric_model{tag}.torch'
    
    #split into train, test
    fsplit = min(max(0.,fsplit),1.) #check range
    b = X.shape[0]        #full batch
    t = int((1-fsplit)*b) #test 
    train_tensors = TensorDataset(X[:b-t],y[:b-t])
    test_tensors = TensorDataset(X[b-t:b],y[b-t:b])    
    train_dataloader = DataLoader(train_tensors, batch_size=batch_size, shuffle=True)
    test_dataloader = DataLoader(test_tensors, batch_size=batch_size, shuffle=True)
    
    #start the optimizer
    optim_args={'params':model.parameters(),'lr':lr}
    optimizer = torch.optim.Adam(**optim_args)
    
    #scheduler to update the learning rate each epoch
    scheduler = ExponentialLR(optimizer, gamma=0.9)
    
    #loss function : this will be converted to RMSE later
    if dosigma:
        criterion = tnn.GaussianNLLLoss()
        print(f'Using gaussian likelihood (NLL) as the loss function')
    else:
        criterion = tnn.MSELoss()
    
    #prepare a file to store the train losses
    trainlogurl=f'{root}/semiparametric_train{tag}.log'
    with open(trainlogurl,'w') as f:
        f.write('epoch\tbatch\ttrain\tloss\n')
    def _updateTrainLog(epoch,batch,istrain,loss):
        with open(trainlogurl,'a') as f:
            f.write(f'{epoch:d} {batch:d} {istrain} {loss:3.4f}\n')
            
    #start the training
    starttime = time.time()  
    ntrainbatches=int((b-t)/batch_size)
    nnoimprov=0
    min_avg_test_loss=None
    for i in range(epochs):

        # Run the training batches
        batch_train_losses=[]
        for ibatch, (xtrain, ytrain) in enumerate(train_dataloader):
            
            # Apply the model
            ypred,sigmaypred = model(xtrain)
            ypred = ypred.reshape(-1,1)
            sigmaypred = sigmaypred.reshape(-1,1)

            if dosigma:
                loss = criterion(ypred,ytrain,sigmaypred)
                batch_train_losses.append( loss.item() )
            else:
                loss = criterion(ypred, ytrain)
                batch_train_losses.append( np.sqrt(loss.item()))
            
            _updateTrainLog(i,ibatch,True,batch_train_losses[-1])
            
            # Update parameters
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            
            if ibatch%400==0:
                print(f'epoch {i} batch {ibatch}/{ntrainbatches} loss={batch_train_losses[-1]:.3f}')
                
        #update learning rate
        scheduler.step()
    
        #run the testing batches
        batch_test_losses = []
        with torch.no_grad():
             
            for b, (xtest, ytest) in enumerate(test_dataloader):

                # Apply the model
                ypredtest,sigmaypredtest = model(xtest)
                ypredtest = ypredtest.reshape(-1,1)
                sigmaypredtest = sigmaypredtest.reshape(-1,1)

                # Update test loss & accuracy for the epoch
                if dosigma:
                    loss = criterion(ypredtest,ytest,sigmaypredtest)
                    batch_test_losses.append( loss.item() )
                else:
                    loss = criterion(ypredtest, ytest)
                    batch_test_losses.append( np.sqrt(loss.item()))
                _updateTrainLog(i,b,False,batch_test_losses[-1])
        
        #report status
        avg_train_loss=np.mean(batch_train_losses)
        avg_test_loss=np.mean(batch_test_losses)
        print(f"epoch {i} average train/test loss is: {avg_train_loss:.3f}/{avg_test_loss:.3f}")
        if min_avg_test_loss is None or avg_test_loss<min_avg_test_loss:
            print(f'average test loss has improved, saving model')
            min_avg_test_loss=avg_test_loss
            nnoimprov=0
            torch.save(model, modelurl)
        else:
            nnoimprov+=1
            print(f'average test loss did not improve after {nnoimprov} epochs')
            if nnoimprov>max_nnoimprov:
                print(f'\t => stop training')
                break
            
    #log results location
    endtime = time.time()
    print(f'Best model stored @ {modelurl}')
    print(f'Test/train losses stored @ {trainlogurl}')
    print(f'Average test loss achieved {min_avg_test_loss:.3f}')
    print(f'Total elapsed time:{(endtime-starttime)/60:3.2f} min')
    
    #return best model url, and log url
    return modelurl,trainlogurl


def predict_semiparametric(X,model,features, full=False):

    """
    runs the model predicition and updates the dataframe with the result
    df - dataframe
    model - semi-parametric model
    full - return both the target as well as the final corrected parametrized features
    """
    
    #eval in small batches
    eval_tensors = TensorDataset(X)
    eval_dataloader = DataLoader(eval_tensors, batch_size=1024, shuffle=False)
    
    
    #turn off dropout, batchnorm etc.
    model.eval()

    #loop over data and evaluate in batches
    R=[]
    sigma=[]
    param_feats=[]
    printfirst=True
    with torch.no_grad():
        for ibatch, xeval in enumerate(eval_dataloader):
            for x in xeval:
                predictions = model._forward(x,full)
                if full:
                    R.append( predictions[0].cpu().numpy() )
                    sigma.append( predictions[1].cpu().numpy() )
                    param_feats.append( predictions[2].cpu().numpy().T ) #needs to be transposed
                else:
                    R.append( predictions[0].cpu().numpy() )
                    sigma.append( predictions[1].cpu().numpy() )
                    
                    
    R=np.concatenate(R, axis=None)
    sigma=np.concatenate(sigma, axis=None)
    if full:
        param_feats=np.concatenate(param_feats,axis=0)
    else:
        param_feats=None
        
    return R,sigma,param_feats