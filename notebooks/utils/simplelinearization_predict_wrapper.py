#!/usr/bin/env python
# coding: utf-8

# # Simple energy linearization predict wrapper
# 
# This notebook wraps the procedure of running the simple energy lineariation model for inference
# 
# Author: P. Silva (psilva@cern.ch)
# 
# Date: 23/03/2022

import os
import glob
import sys
import datetime
import pandas as pd
import numpy as np
from energy_regression_tools import *
from simplelinearization_training_wrapper import prepareForCalib
from tqdm import tqdm
import mplhep as hep
plt.style.use([hep.style.CMS, hep.style.firamath])

def runPrediction(inurl,modelurl,outurl,maxfcee,plotdir,plottag,**kwargs):

    """runs the prediction over a datase. The arguments are:
    * `inurl` - the base file with the events
    * `modelurl` - the calibration file
    * `outurl` - the output file
    * `maxfcee` - the max. fractional energy in CE-E (or None)
    * `plotdir` - the output directory for intermediate plots
    * `plottag` - the unique name for the plots of a given prediction set
    """
    
    #read the summary
    df=pd.read_feather(inurl)
    df,_=prepareForCalib(df,maxfcee)
    
    #read the coefficients and apply correction
    reg_coeffs=pd.read_feather(modelurl)    
    df=applyL1Correction(df,reg_coeffs)

    #compute the final residuals
    df,res_df=doResiduals(df,outdir=plotdir,plot_tag=plottag)
    res_df_url=outurl.replace('.feather','_l1residuals.feather')
    res_df.reset_index(inplace=True)
    res_df.to_feather(res_df_url)
    
    #final dataframe
    df.reset_index(inplace=True)
    local_outurl=os.path.basename(outurl)
    df.to_feather(local_outurl)
    os.system(f'mv -v {local_outurl} {outurl}')
    
    del df

        
def submitRunPrediction(predict_sets, method):

    """
    takes care of running a prediction `method` over a set of files defined by `predict_sets`
    `predict_sets` is a list of tuples define as (inputurl, caliburl, maxfCEE)
    being:
    * `inputurl` either a file or a wildcard for a set of files
    * `caliburl` either a file or None
    * `maxfCEE` the max. fractional energy in CE-E (or None)
    `method` is the method to run. It is assumed to take the following arguments sequentially
    * `inurl` - the base file with the events
    * `modelurl` - the calibration file
    * `outurl` - the output file
    * `maxfcee` - the max. fractional energy in CE-E (or None)
    * `plotdir` - the output directory for intermediate plots
    * `plottag` - the unique name for the plots of a given prediction set
    These arguments are automatically filled in this method
    """
    
    #loop over sets and build tasks to run
    for (inputs, basemodelurl, maxfcee, kwargs_dict) in predict_sets: 

        #gather all files
        inputs = glob.glob(inputs) if '*' in inputs else [inputs]
        for inurl in inputs:

            if not os.path.isfile(inurl):
                print(f'Could not find input {inurl}')
                continue            
            indir=os.path.dirname(inurl)
            fname=os.path.splitext(os.path.basename(inurl))[0]

            #get the calibration to apply: if not given build the one expected
            if basemodelurl is None:
                seltag='' if maxfcee is None else f'_maxfcee{maxfcee:3.2f}'
                seltag=seltag.replace('.','p')
                modelurl=f'{indir}/training/l1regcoeffs_{fname}{seltag}.feather'
            else:
                modelurl=basemodelurl
                if '*' in modelurl:
                    modelurl=modelurl.replace('{}',f'*{fname}_')
                    modelurl=modelurl.replace('energysums_ana','')                    
                    modellist = glob.glob(modelurl)
                    modelurl=modellist[0] if len(modellist)==1 else None
            if modelurl is None or not os.path.isfile(modelurl):
                print(f'Could not find calibration model {modelurl}')
                continue
                    
            #prepare the outputs
            cname=os.path.splitext(os.path.basename(modelurl))[0]
            caliboutname=f'{fname}_{cname}'
            plotdir=indir+'/predictions/plots'
            os.system(f'mkdir -p {plotdir}')
            outurl=os.path.dirname(inurl)+f'/predictions/{caliboutname}.feather'
            
            method(inurl,modelurl,outurl,maxfcee,plotdir,caliboutname,**kwargs_dict)
            print('Ran predictions for')
            print(f'\tInput: {inurl}')
            print(f'\tModel: {modelurl}')
            print(f'\tmax fCEE: {maxfcee}')
            print(f'\tOutput: {outurl}')
            print(f'\tPlots: {plotdir}')            
            print(f'\tTag: {caliboutname}')
            print('')


def main():

    FIXLC1='/eos/cms/store/group/dpg_hgcal/comm_hgcal/psilva/K0L_studies/2024Apr27_lc1/summaries/K0LFixedEta'
    FIXLC2='/eos/cms/store/group/dpg_hgcal/comm_hgcal/psilva/K0L_studies/2024Apr27_lc2/summaries/K0LFixedEta'

    predict_sets=[
        (f'{FIXLC1}/energysums*.feather', None, None, {}),
        (f'{FIXLC2}/energysums*.feather', None, None, {}),
    ]

    submitRunPrediction(predict_sets, runPrediction)


if __name__ == '__main__':
    sys.exit(main())
