import numpy as np
import pandas as pd
from sklearn.linear_model import RidgeCV
from scipy.interpolate import UnivariateSpline
import matplotlib.pyplot as plt
import mplhep as hep
from scipy.optimize import curve_fit
from scipy.stats import gamma 
from matplotlib.colors import LogNorm as ColorsLogNorm
from torch.utils.data import DataLoader
import mplhep as hep
plt.style.use([hep.style.CMS, hep.style.firamath])
import glob

def computeSigmaEff(x,delta=68):
    
    """compute percentiles and min interval that holds a given delta"""
    q=np.linspace(1,100,100)
    m=np.percentile(x, q=q)
    dm=np.array( [m[delta+i]-m[i] for i in range(0,100-delta)] )
    idx=dm.argsort()[0]
    return 0.5*dm[idx]

def doL1Regression(df,x_cols,y_target='genEn',useRidgeCV=True,outdir=None,plot_tag=''):

    """
    does the first level calibration finding the optimal coefficients
    which can be used to sum up the different columns to estimate the target
    df - dataframe which will be added with two columns: en_l0 (raw sum) and en_l1 (weighted sum)
    x_cols - the columns to use in the estimation of the target
    y_target - target to regress
    returns the coefficients to apply for L1, as a dict {eta:{cols:[],beta:[]}}
    """
    
    print('[doL1] running regularized L1 calibration')
        
    #initiate the L1 estimation
    df['en_l1_optim']=-1
    
    #regularization parameters to scan with cross validation
    lambda_scan=np.logspace(-2.,1.,5)
    if useRidgeCV:
        lambda_scan=np.logspace(-4.,2.,10)
        
    #iterate over different eta,et bins
    summary=[]
    for group, data in df.groupby(['eta_cat','en_cat']):

        #MC truth
        genEn=data['genEn'].mean()
        genEnRange=(data['genEn'].min(),data['genEn'].max())
        genEt=data['genEt'].mean()
        genEta=data['genAbsEta'].mean()
        genEtaRange=(data['genAbsEta'].min(),data['genAbsEta'].max())
        
        #features and target
        X = data[x_cols].to_numpy()
        X0 = data['en_l0'].to_numpy()
        
        R=np.abs(X0/data[y_target]-1)
        fit_mask=(R<0.5)
        X0_mean=X0[fit_mask].mean() #average reconstructed energy (within fit range)
        
        #print('\t@ ET={:3.2f} eta={:3.2f} with {} showers <E(L0)>={:3.2f}'.format(genEt,genEta,data.shape[0],X0_mean))
        
        best_reg=None
        coeffs=[]
        cv_results=[]
        try:
                
            if useRidgeCV:
                Y = data[y_target].to_numpy()-X0
                rcv = RidgeCV(alphas=lambda_scan,
                              fit_intercept=False,
                              scoring='neg_mean_squared_error',
                              store_cv_values=True,
                              cv=None)
                rcv.fit(X[fit_mask],Y[fit_mask])
                best_reg=rcv.alpha_
                coeffs=rcv.coef_.tolist()
                test_mean_score=-rcv.cv_values_.mean(axis=0)
                test_std_score=rcv.cv_values_.std(axis=0)
                cv_results=np.hstack([lambda_scan.reshape(-1,1),
                                      test_mean_score.reshape(-1,1),
                                      test_std_score.reshape(-1,1)])
                cv_results=pd.DataFrame( cv_results, columns=['lambda','test_score','test_score_std'] )
                
                #update the L1 estimation with the best fit
                data['en_l1_optim'] = X0+rcv.predict(X)
                                               
            else:
                Y = data[y_target].to_numpy()
                lrcv=CustomLossLinearRegressionModelCV(lambda_scan=lambda_scan)
                best_model=lrcv.cross_validate(X=X[fit_mask],Y=Y[fit_mask],X0=X0[fit_mask])
                best_reg=lrcv.best_lam
                coeffs=best_model.beta.tolist()
                cv_results=lrcv.cv_scores
                
                #update the L1 estimation with the best model
                data['en_l1_optim']=best_model.predict(X,X0)
            
            #copy to original dataframe
            mask=(df['eta_cat']==group[0]) & (df['en_cat']==group[1])
            df.loc[mask,'en_l1_optim']=data['en_l1_optim']
 
            #add a scaled version of the feature
            for ifeat,feature in enumerate(x_cols):
                optim_feature='optim_{}'.format(feature)
                if not optim_feature in df.columns:
                    df[optim_feature]=df[feature]
                df.loc[mask,optim_feature] = (1.+coeffs[ifeat])*data[feature]
            
            
        except Exception as e:
            print('Failed to run fit with: {}'.format(e))
            continue
            
        summary.append( [group[0],group[1],data.shape[0],genEn,genEt,genEta,X0_mean,best_reg] + coeffs)
        
        #do some plotting
        if not outdir is None:
            outname='{}/rawvsoptim_{}_{}'.format(outdir,*group,plot_tag)
            compareResponse(data,outname=outname)
        
            outname='{}/testscore_{}_{}'.format(outdir,*group,plot_tag)
            showScoreVersusRegularization(cv_results=cv_results,
                                          rlabel=r'{:3.0f}<E/GeV<{:3.0f}    {:3.2f}<$|\eta|$<{:3.2f}'.format(*genEnRange,*genEtaRange),
                                          outname=outname)
    
    #save calibration summary
    calib_cols=['eta_cat','en_cat','N','genEn','genEt','genAbsEta','en_l0_mean','lambda']
    calib_cols+=['b_{}'.format(x) for x in x_cols]
    calib_coeff=pd.DataFrame(summary,
                             columns=calib_cols)
    
    return calib_coeff


def param_func(x, a, b, c, d):
    
    """function used to regularize correction"""

    return a+b/np.sqrt(x+1)+c/(x+1)+d*x


def regularizeCalibCoeffs(coeffs,outdir,plot_tag=''):
    
    """regularizes with a parametric function the calibration constants"""

    #parametric functon and list of coefficients
    coeff_param=[]

    #some common formatting options
    style_dict={}
    x_features=[x for x in coeffs.columns if 'b_' in x]
    for x in x_features:
        color='black'
        ls='-'
        marker='o'
        if 'Si100' in x  : color='indigo'
        if 'Si200' in x  : color='royalblue'
        if 'Si300' in x  : color='forestgreen'
        if 'Sci'   in x  : color='darkorange'
        if 'fine'  in x  : ls,marker = '--','v'
        if 'coarse' in x : ls,marker = '--','^'
        style_dict[x]=(color,ls,marker)
    

    #loop on each pseudo-rapidity
    for eta_cat in coeffs['eta_cat'].unique():

        mask=(coeffs['eta_cat']==eta_cat)        
        eta=coeffs[mask]['genAbsEta'].mean()
        #etaRange=(coeffs[mask]['genAbsEta'].min(),coeffs[mask]['genAbsEta'].max())
        
        fig,ax=plt.subplots(figsize=(8,8))
        for i,feat in enumerate(x_features):
            
            x=coeffs[mask]['en_l0_mean'].to_numpy()
            y=coeffs[mask][feat].to_numpy()
            
            if len(y)<4:
                print(f'Too many parameters to regularize with {len(y)} points: skipping eta={eta:3.1f}')
                continue
       
            #filter out when:
            # - there is too much spread (fit was not sensitive most probably and diverged picking up on noise)
            # - the average correction is 0 (it's irrelevant)
            # - very strong variations from the parameterized function
            mean=np.abs(y.mean())
            spread=y.std()
            popt=np.zeros(4)
            try:
                mask_for_fit = np.abs(y-mean)<1.5*spread
                popt, pcov = curve_fit(param_func,x[mask_for_fit],y[mask_for_fit])
                p = lambda x : param_func(x,*popt)            
                min_var=np.min(p(x))
                max_var=np.max(p(x))
            except:
                print(f'Regularization fit did not converge for eta={eta} i={i}, setting all to 0')
                min_var=-10
                max_var=+10
            keepParam = False if min_var<-0.5 or max_var>1 or spread>0.4 or mean<1e-3 else True
            #keepParam=(spread<0.5)
            if not keepParam:        
                popt=np.zeros(4)
            coeff_param.append( [eta_cat,feat,np.min(x),np.max(x)]+popt.tolist() )

            
            if eta_cat==0:
                print(feat,x,y,keepParam,min_var,max_var,spread,mean)
            
            
            if not keepParam: continue

            ci,ls,mk=style_dict[feat]
            ylabel=f'weight( {feat} )'
            ylabel=ylabel.replace('b_sumen_',r'$\sum$')
            ax.errorbar(x,y,ls='none',color=ci,label=ylabel,marker=mk) 
            ax.plot(x,p(x),ls,color=ci)               
                
        ax.set_xlabel('<Reconstructed raw energy> [GeV]')
        ax.set_ylabel('Weight')
        ax.set_ylim(-0.2,1.5)
        ax.legend(fontsize=12)
        ax.grid()
        hep.cms.label(loc=0,data=False,ax=ax,rlabel=r'$<|\eta|>={:3.2f}$'.format(eta))

        if outdir is None:
            plt.show()
        else:
            outname=f'{outdir}/featureweights_{eta_cat}{plot_tag}.png'
            plt.savefig(outname)
            plt.close()
    
    #return the parameterized coefficients
    coeff_param = pd.DataFrame(coeff_param,
                               columns=['eta_cat','feature','min','max']+['p{}'.format(i) for i in range(4)])
    return coeff_param
    
    
def applyL1Correction(df,coeff_param):
    
    """
    uses the regularized coefficients for the weights of the features 
    to compute the correction over the raw data estimate
    adds a new column to the table called en_l1
    """
    
    x_features=coeff_param['feature'].unique()
    
    def _applyL1(row, feature_coeffs_dict, debug=False):

        eta_cat=int(row['eta_cat'])
        en_l0=row['en_l0']
        if debug: print(eta_cat,row['genEn'],en_l0)
        delta=0.
        for bfeature in x_features:
            
            ckey=(eta_cat,bfeature)
            if not ckey in feature_coeffs_dict: continue
            
            feature=bfeature[2:]
            x=row[feature]
            
            popt=feature_coeffs_dict[ckey]['popt']
        
            #set the energy to be used 
            #(saturate estimate to the min/max used in the parameterization if extension is needed)
            en_l0_min=feature_coeffs_dict[ckey]['min']
            en_l0_max=feature_coeffs_dict[ckey]['max']
            en_l0_reg=min(max(x,en_l0_min),en_l0_max)
        
            w=param_func(en_l0_reg, *popt)
            delta += w*x
            if debug: print('\t',feature,x,en_l0_reg,popt,w)
        if debug : print('---->',en_l0+delta)

        return en_l0+delta
 
    #transform to a dict for faster access when iterating over rows
    feature_coeffs_dict={}
    for _,row in coeff_param.iterrows():
        key=(row['eta_cat'],row['feature'])
        feature_coeffs_dict[key]={'min':row['min'],
                                  'max':row['max'],
                                  'popt':[row[x] for x in ['p0','p1','p2','p3']]}

    #apply
    df['en_l1'] = df['en_l0']
    df['en_l1'] = df.apply( lambda row : _applyL1(row,feature_coeffs_dict) , axis=1)
    
    return df
    
def doResiduals(df,y_pred='en_l1',y_target='genEn',outdir=None,plot_tag='',ylim=(0.9,2)):
    
    """
    profiles the target - E(gen) as function of the reconstructed energy E(rec)
    the profile is interpolated and used to derive the residual correction to apply
    the list of masks is used to derive individual residual corrections for different regions
    a new column is added to the the dataframe including the final residual - corrected estimator
    it returns a dict with the residuals and their parameterization
    """
        
    residuals={}
    
    #compute the residuals and group per (eta,ET) category
    res_df      = df[['en_cat','eta_cat',y_pred,'genAbsEta']].copy()
    mask_nulls = (df[y_pred]<=0)
    if mask_nulls.sum()>0:
        print(f'Masking {mask_nulls.sum()} nulls / {res_df.shape[0]} entries... you may want to check')
        print('Here is a printout of the first')
        print(res_df[mask_nulls].head())
        res_df=res_df[~mask_nulls]
    res_df['R'] = df[y_target]/df[y_pred] 

    agg_dict={'R':[np.median,np.std,'count'],y_pred:[np.mean,np.std],'genAbsEta':[np.mean]}
    res_summary=res_df.groupby(['eta_cat','en_cat'],as_index=False).agg(agg_dict)
    res_summary.reset_index(inplace=True)
    res_summary.columns = [ '_'.join(col).rstrip('_') for col in res_summary.columns.values ]
    res_summary['R_median_unc']=res_summary['R_std']/np.sqrt(res_summary['R_count'])
    res_summary[y_pred+'_mean_unc']=res_summary[y_pred+'_std']/np.sqrt(res_summary['R_count'])
    res_summary=res_summary[res_summary['R_count']>50] #at least 50 showers
    
    #parameterize the residuals (spline based) and compute the final estimator
    df[y_pred+'_res']=0.
    fig,ax=plt.subplots(figsize=(8,8))
    ebar_style={'marker':'o','elinewidth':1,'capsize':1,'ls':'none'}
    xs=np.logspace(np.log10(res_summary[y_pred+'_mean'].min()),
                   np.log10(res_summary[y_pred+'_mean'].max()),
                   200)
    for group, data in res_summary.groupby('eta_cat'):

        x    = data[y_pred+'_mean'].values
        idx2sort=x.argsort()
        xerr = data[y_pred+'_mean_unc'].values
        y    = data['R_median'].values
        yerr = data['R_median_unc'].values
        eta  = data['genAbsEta_mean'].mean()
        ebar = plt.errorbar(x,y,xerr=xerr,yerr=yerr, label=r'$|\eta|$={:3.2f}'.format(eta),**ebar_style)
         
        spl = UnivariateSpline(x[idx2sort], y[idx2sort], w=1./(yerr[idx2sort]**2), k=1, ext=3)   
        plt.plot(xs,spl(xs),color=ebar[0].get_color())
        
        mask_df=(df['eta_cat']==group)
        df.loc[mask_df,y_pred+'_res']=spl( df[mask_df][y_pred] )*df[mask_df][y_pred]

    ax.grid()
    ax.legend(fontsize=14,loc='upper right') #loc='center left', bbox_to_anchor=(1, 0.5))
    ax.set_ylabel(r'Median $E_{0}~/~E_{rec}$')
    ax.set_xlabel('Median reconstructed energy [GeV]')
    ax.set_xscale('log')    
    ax.set_ylim(*ylim)
    hep.cms.label(loc=0,data=False,ax=ax,rlabel='')
    plt.tight_layout()
    if outdir is None:
        plt.show()
    else:
        outname=f'{outdir}/residuals_{y_pred}{plot_tag}.png'
        plt.savefig(outname)
        plt.close()  

    return df, res_summary


def countuncertainties(n,alpha = 1 - 0.6827):
    L = np.where(n==0,0.,gamma.ppf(alpha/2,n))
    U = gamma.ppf(1-alpha/2,n+1)
    n_err=np.stack( [n-L,U-n], axis=1)
    return n_err


def compareResponse(data, 
                    bins=np.linspace(-1,1.5,100),
                    estimators=['en_l0','en_l1_optim'], 
                    estimator_titles=['Raw','Best fit'],
                    outname=None,
                    showMedian=False,
                    logy=True,
                    normTo=-1):
    
    """plots the response of different estimators"""

    plt.style.use([hep.style.CMS, hep.style.firamath])
    fig,ax=plt.subplots(figsize=(10,8))
       
    histos=[]
    for i, est in enumerate(estimators):
        est_title = estimator_titles[i]
        r=data[est]/data['genEn']-1.
        r_mean=r.median() if showMedian else r.mean() 
        seff=computeSigmaEff(r)
        label='{} $\sigma_{{eff}}/E_0={:3.2f}$'.format(est_title,seff)
        fmt_args={'bins':bins,'histtype':'step','label':label,'linewidth':2}    
        cts,_,patches = ax.hist(r,**fmt_args)
        if normTo>0:
            cts *= normTo / cts.sum()
        color=patches[0].get_ec()
        histos.append( (cts,color) )
        ax.plot([r_mean]*2,[0,np.max(cts)],ls='--',color=color, lw=2)
    
    ax.legend(loc='upper right')
    ax.set_ylabel('Events' if normTo<0 else 'Events [A.U.]' )
    ax.set_xlim(bins[0],bins[-1])
    ax.set_xlabel(r'Response = $E/E_0-1$')
    ax.grid()
        
    genEnRange=(data['genEn'].min(),data['genEn'].max())
    avgEta=np.mean(data['genAbsEta'])
    hep.cms.label(loc=2,
                  data=False,
                  ax=ax,
                  rlabel=r'{:3.0f}<E/GeV<{:3.0f}  $<|\eta|>$={:3.2f}'.format(*genEnRange,avgEta))
    if logy:
        ax.set_yscale('log')
        ax.set_ylim(0.5,ax.get_ylim()[1]*4)
    fig.tight_layout()
    
    if outname:
        plt.savefig(f'{outname}_log.png')
        plt.close()
    else:
        plt.show()
        

def compareDifferentDatasets(xlist, titlelist,xlabel, rlabel,
                             bins=np.linspace(-1,1.5,40),
                             outname=None,
                             showMedian=False,
                             doRatio=False,
                             logy=True,
                             normTo=-1):
    
    """plots the response of different estimators"""

    plt.style.use([hep.style.CMS, hep.style.firamath])
    if doRatio:
        fig,(ax,axr)=plt.subplots(2,1,figsize=(10,12),gridspec_kw={'height_ratios': [1, 0.3]})
    else:
        fig,ax=plt.subplots(figsize=(10,8))
       
    histos=[]
    for i,x in enumerate(xlist):
        title = titlelist[i]
        x_mean=x.median() if showMedian else x.mean() 
        seff=computeSigmaEff(x)
        label='{} $\sigma_{{eff}}/E_0={:3.2f}$'.format(title,seff)
        fmt_args={'bins':bins,'histtype':'step','label':label,'linewidth':2}    
        if i==0:
            fmt_args['linewidth']=3
            fmt_args['color']='k'
        cts,_,patches = ax.hist(x,**fmt_args)
        kfact = normTo / cts.sum() if normTo>0 else 1
        cts *= kfact
        color=patches[0].get_ec()
        histos.append( (cts,kfact,color) )
        ax.plot([x_mean]*2,[0,np.max(cts)],ls='--',color=color,lw=2)
    
    ax.legend(loc='upper right')
    ax.set_ylabel('Events' if normTo<0 else 'Events [A.U.]')
    ax.set_xlim(bins[0],bins[-1])
    if not doRatio: 
        ax.set_xlabel(xlabel)
    ax.grid()
    
    if doRatio:
        xcen=0.5*(bins[1:]+bins[0:-1])
        
        #represent with a band the intrinsic uncertainty of the reference
        ref_cts=histos[0][0]
        kfact = histos[0][1]
        mask_ratio=(ref_cts>0)
        ref_cts_unc=countuncertainties(ref_cts/kfact)*kfact
        fill_style={'step':'post','facecolor':'lightgray','alpha':0.2,'edgecolor':'black', 'linewidth':0, 'hatch':'///'}
        y1=1-np.divide(ref_cts_unc[:,0],ref_cts, out=np.zeros_like(ref_cts), where=mask_ratio)
        y2=1+np.divide(ref_cts_unc[:,1],ref_cts, out=np.zeros_like(ref_cts), where=mask_ratio)
        axr.fill_between(x=xcen[mask_ratio], 
                         y1=y1[mask_ratio], 
                         y2=y2[mask_ratio],
                         **fill_style
                        )

        #show the ratios
        for i in range(1,len(histos)):
            cts_ratio = np.divide(histos[i][0],ref_cts,out=np.zeros_like(ref_cts),where=mask_ratio)
            color=histos[i][2]
            ebar_style={'marker':'o','markersize':10,'elinewidth':1,'capsize':1,'color':color,'ls':'none'}
            axr.errorbar(xcen[mask_ratio],cts_ratio[mask_ratio],**ebar_style)
        axr.set_xlabel(xlabel)
        axr.set_ylabel(fr'Ratio to {titlelist[0]}',fontsize=14)
        axr.set_xlim(bins[0],bins[-1])
        axr.set_ylim(0.3,1.7)
        axr.plot([bins[0],bins[-1]],[1,1],ls='--',color='gray')
        axr.grid()
    
    hep.cms.label(loc=2, data=False, ax=ax, rlabel=rlabel)
    if logy: 
        ax.set_yscale('log')
        ax.set_ylim(0.5,ax.get_ylim()[1]*20)
    
    fig.tight_layout()
    
    if outname:
        plt.savefig(f'{outname}.png')
        plt.close()
    else:
        plt.show()

        
def showScoreVersusRegularization(cv_results,rlabel,outname=None):

    """makes a plot of the test and train scores as function of the regularization parameter"""

    fig,ax=plt.subplots(figsize=(10,8))
    ax.errorbar(cv_results['lambda'],cv_results['test_score'],yerr=cv_results['test_score_std'],label='test')
    if 'train_score' in cv_results.columns:
        ax.errorbar(cv_results['lambda'],cv_results['train_score'],yerr=cv_results['train_score_std'],label='train')
    ax.set_xlabel(r'Regularization $\lambda$')
    ax.set_ylabel('Score')
    ax.set_xscale('log')
    hep.cms.label(loc=0,data=False,ax=ax,rlabel=rlabel)
    ax.grid()
    ax.legend()
    plt.tight_layout()
    if outname:
        plt.savefig(f'{outname}.png')
        plt.close()
    else:
        plt.show()
        
        


def profileRelativeContributionsToEnergy(df,sum_name='',doScivsSi=False,outname=None):
    
    """
    makes the profile of the relative contribution of a sum to the total energy estimator
    sample - selects the sample
    outname - output file name
    """
    
    section_list=[
        (r'CE-E',['CEESi{}'.format(thick) for thick in [100,200,300]],None),
        (r'$CE-H_{fine}$',['CEHfineSi{}'.format(thick) for thick in [100,200,300]] + ['CEHScifine'],None),
        (r'$CE-H_{coarse}$',['CEHcoarseSi{}'.format(thick) for thick in [100,200,300]]+ ['CEHScicoarse'],None),
        #(r'$CE-H_{last~3}$',['CEHlast3'],10),
    ]
    if doScivsSi:
        section_list=[
            (r'Si',
             ['CEESi{}'.format(thick) for thick in [100,200,300]]+
             ['CEHfineSi{}'.format(thick) for thick in [100,200,300]]+
             ['CEHcoarseSi{}'.format(thick) for thick in [100,200,300]],
             None),
            (r'SiPM-on-tile',['CEHScifine','CEHScicoarse'],None),
        ]
    
    ebar_style={'marker':'o','elinewidth':1,'capsize':1,'ls':'--'}
    fill_style={'step':'post','facecolor':'lightgray','alpha':0.5,'edgecolor':'black', 'linewidth':0, 'hatch':'///'}
    
    #do plots per eta slice
    for eta_cat in df['eta_cat'].unique():
        
        fig, ax = plt.subplots(figsize=(8,8))
        
        #build each section profile (average) as function of energy
        ysum,ysumerr=[],[]
        for section_title,sections,kfact in section_list: 
        
            if kfact : section_title = '{} x {:3.0f}'.format(section_title,kfact)
    
            x,y,yerr=[],[],[]
            for en_cat in df['en_cat'].unique():
                
                mask=(df['eta_cat']==eta_cat) & (df['en_cat']==en_cat)
                n=np.sum(mask)
                if n<10 : continue
                en=df[mask]['genEn']
                x.append(en.mean())

                cols_to_sum=['sumen_{}{}'.format(s,sum_name) for s in sections]
                den='en_l0' if sum_name=='' else 'en_l1_res'
                f    = df[mask][cols_to_sum].sum(axis=1)/df[mask][den]
                fgen = df[mask][cols_to_sum].sum(axis=1)/en
              
                kfact=1 if kfact is None else kfact
                y.append(kfact*f.mean())
                yerr.append(kfact*f.std()/np.sqrt(n))
                
                iy=len(y)
                if len(ysum)<iy:
                    ysum.append(fgen.mean())
                    ysumerr.append( (fgen.std()**2)/n )
                else:    
                    ysum[iy-1]    += fgen.mean()
                    ysumerr[iy-1] += (fgen.std()**2)/n 
                
            x=np.array(x)
            y=np.array(y)
            yerr=np.array(yerr)
            sort_idx=np.argsort(x)
            ax.errorbar(x=x[sort_idx],y=y[sort_idx],yerr=yerr[sort_idx],label=section_title, **ebar_style)
           
        ysum=np.array(ysum)
        ysumerr=np.sqrt(np.array(ysumerr))
        #ax.fill_between(x=x[sort_idx],
        #                y1=(ysum-ysumerr)[sort_idx],
        #                y2=(ysum+ysumerr)[sort_idx],
        #                **fill_style,
        #                label=r'$\Sigma E_i~/~E_{gen}$')

        ax.legend()
        ax.grid()
        ax.set_xlabel('Energy [GeV]')
        ax.set_ylabel(r'$<\Sigma E_i~/~E_{total}>$')
        ax.set_ylim(0,1.3)
        ax.set_xscale('log')
        eta=df[df['eta_cat']==eta_cat]['genAbsEta'].mean()
        hep.cms.label(loc=0,
                      data=False,
                      ax=ax,
                      rlabel=r'$|\eta|$={:3.2f}'.format(eta))
        fig.tight_layout()
    
        if outname:
            plt.savefig('{}_eta{}.png'.format(outname,eta_cat))
            plt.close()
        else:
            plt.show()


def profileRelativeEnergyBalance(df,sum_name='',outname=None, selen_cats=[7,15]):
    
    """
    makes the profile of the relative contribution of a sum to the total energy estimator
    sample - selects the sample
    outname - output file name
    """
    
    section_sums={
        'CEE':['sumen_CEESi{}{}'.format(thick,sum_name) for thick in [100,200,300]],
        'CEHfine':['sumen_CEHfineSi{}{}'.format(thick,sum_name) for thick in [100,200,300]] + ['sumen_CEHScifine'+sum_name],
        'CEHcoarse':['sumen_CEHcoarseSi{}{}'.format(thick,sum_name) for thick in [100,200,300]]+ ['sumen_CEHScicoarse'+sum_name],
    }
        
    f_bins=np.linspace(0,1.5,30)
    
    #do plots per eta/et slice (but do every 3 in Et)
    for (eta_cat,en_cat), group in df.groupby(['eta_cat','en_cat']):
        if not en_cat in selen_cats: continue    
        if group.shape[0]<10 : continue

        en=group['genEn']    
                
        f_ceesi      = group[section_sums['CEE']].sum(axis=1)/en
        f_cehfine    = group[section_sums['CEHfine']].sum(axis=1)/en
        f_cehcoarse  = group[section_sums['CEHcoarse']].sum(axis=1)/en
        f_ceecehfine = f_ceesi+f_cehfine
        f_ceh        = f_cehfine + f_cehcoarse
            
        #inclusive distributions
        inc_fig, inc_ax = plt.subplots(figsize=(8,8))
        kwargs={'bins':f_bins,'histtype':'step','density':True,'linewidth':3}
        inc_ax.hist(f_ceesi,    label=r'$E_{CE-E}~/~E_{gen}$',         **kwargs)
        inc_ax.hist(f_cehfine,  label=r'$E_{CE-H_{fine}}~/~E_{gen}$',  **kwargs)
        inc_ax.hist(f_cehcoarse,label=r'$E_{CE-H_{coarse}}~/~E_{gen}$', **kwargs)
        inc_ax.grid()
        inc_ax.set_xlabel('Energy fraction')
        inc_ax.set_ylabel('PDF')
        cat_label=r'$E$={:3.0f} GeV $|\eta|$={:3.1f}'.format(group['genEn'].mean(),group['genAbsEta'].mean())
        hep.cms.label(loc=0,data=False,ax=inc_ax,rlabel='') 
        inc_ax.legend(title=cat_label)
        inc_ax.set_ylim(0.1,40)
        inc_ax.set_yscale('log')
        inc_fig.tight_layout()
        if outname:
            plt.savefig('{}proj_ien{}_ieta{}.png'.format(outname,en_cat,eta_cat))
            plt.close()
        else:
            plt.show()
            
        #2D-distributions
        cmin,cmax=0.5,200
        fig, (ax1,ax2) = plt.subplots(1,2,figsize=(14,7))
        h1=ax1.hist2d(f_ceesi,f_ceh,    bins=(f_bins,f_bins), cmap='jet', density=True, norm=ColorsLogNorm(),cmin=cmin)
        ax1.plot([1,0],[0,1])
        ax1.grid()
        ax1.set_xlabel(r'$E_{CE-E}~/~E_{gen}$')
        ax1.set_ylabel(r'$E_{CE-H}~/~E_{gen}$')
        hep.cms.label(loc=0,data=False,ax=ax1,rlabel='') 
        h1[-1].set_clim(vmin=cmin, vmax=cmax)
        fig.colorbar(h1[3], ax=ax1)
            
        h2=ax2.hist2d(f_ceecehfine, f_cehcoarse, bins=(f_bins,f_bins), cmap='jet', 
                      density=True, norm=ColorsLogNorm(),cmin=cmin)
        ax2.plot([1,0],[0,1])
        ax2.grid()
        ax2.set_xlabel(r'$(E_{CE-E}+E_{CE-H_{fine}})~/~E_{gen}$')
        ax2.set_ylabel(r'$E_{CE-H_{coarse}}~/~E_{gen}$')
        h2[-1].set_clim(vmin=cmin, vmax=cmax)
        fig.colorbar(h2[3], ax=ax2)
                
        ax2.text(1.05,1.0,
                 cat_label,
                 transform=ax2.transAxes,
                 verticalalignment='bottom',
                 horizontalalignment='right')
            
        fig.tight_layout()
            
        if outname:
            plt.savefig('{}_ien{}_ieta{}.png'.format(outname,en_cat,eta_cat))
            plt.close()
        else:
            plt.show()
                
def gaussian_core(x,*p):
    
    """just a gaussian function..."""
    
    k, mu, sigma = p
    return k*np.exp(-0.5*(((x-mu)/sigma)**2))


def getMedianAndEffSigma(x,cl=68):
    
    """
    estimates the median and the quantiles for a 68%CI around it
    returns the median, median uncertainty, effective width and the unc. on eff. width
    the uncertainties are computed using a gaussian approximation
    (a la ROOT https://root.cern.ch/doc/master/TH1_8cxx_source.html#l07467)
    """
    
    #q1=(100-cl)/2
    #q2=100-q1
    #p=np.percentile(x, [q1,50,q2])
    sigma=np.std(x,axis=0)
    n=x.shape[0]
    
    #median and uncertainty on median
    #r=p[1]
    r=np.median(x)
    unc_r=1.253*sigma/np.sqrt(n)
    
    #eff. width and uncertainty on it 
    #wid_r=0.5*(p[2]-p[0])
    wid_r=computeSigmaEff(x,cl)
    unc_wid_r=sigma/np.sqrt(2*n)
    
    return [r,unc_r,wid_r,unc_wid_r]


def getGaussianCoreParameters(x,xbins):
    
    """
    fits the gaussian core using only the values within effective sigma (68% CI)
    returns the fitted parameters and their errors
    """
    
    median,median_unc,sigma_eff,sigma_eff_unc=getMedianAndEffSigma(x)
    
    #bin and fit
    cts,bins=np.histogram(x, bins=xbins)    
    p0 = [np.sum(cts), median, sigma_eff]
    bin_centres = (bins[:-1] + bins[1:])/2
    fit_mask=(bin_centres>median-sigma_eff) & (bin_centres<median+sigma_eff)
    nevts=cts[fit_mask].sum()

    if nevts.sum()>50:
        try:
            maxfev=500*(nevts+1)
            popt, covar = curve_fit(gaussian_core, bin_centres[fit_mask], cts[fit_mask], p0=p0,maxfev=maxfev)
            return popt.tolist() + np.sqrt(np.diagonal(covar)).tolist() + [True]
        except Exception as e:
            print(e)
            pass

    print('Found too few events to fit {}, or fit failed, returning effective values'.format(nevts))
    return [nevts,median,sigma_eff,np.sqrt(nevts),median_unc,sigma_eff_unc,False]

                
def resol2_func_ix(x, a, b, c):
    
    """standard relative energy resolution function with stochastic, noise and constant terms"""
    
    return (a**2)*x + (b*x)**2 +c**2
    
def resol_func(x, a, b, c):
    return np.sqrt((a/np.sqrt(x))**2+(b/x)**2+c**2)
    
    
def showResolutionFit(samples,
                      levels=['l1_res'],
                      levelsTitle=None,
                      bounds=[(0.15,0.,0.),(2.0,0.1,0.2)],
                      addLegend=True,
                      title=None,
                      outname=None,
                      doRatio=False,
                      doGaussianCore=True,
                      ratioran=(0.7,1.4),
                      minGenEn=0,
                      maxGenEn=None,
                      maxfcee=None,
                      cl=68,
                      nofit=False
                      ):

    """compares the resolutions and performs a fit of the stochastic, noise and constant term"""
        
    #loop over samples and build the resolution summary
    resols=[]
    for ikey,(dftitle,url) in enumerate(samples):
        url = glob.glob(url)[0] if '*' in url else url            
        df=pd.read_feather(url)
        df=df[df['genEn']>minGenEn]
        if not maxGenEn is None:
            df=df[df['genEn']<maxGenEn]
        if not maxfcee is None:
            df=df[df['fcee']<maxfcee]

        #loop over eta/et
        for eta_cat in df['eta_cat'].unique():
            for en_cat in df['en_cat'].unique():
                mask=(df['eta_cat']==eta_cat) & (df['en_cat']==en_cat)       
                if np.sum(mask)<100 : continue      
                sel_df=df[mask]
                
                avg_eta=sel_df['genAbsEta'].mean()
                avg_et=sel_df['genEt'].mean()
                avg_en=sel_df['genEn'].mean()
                
                for lvl in levels:
                    x=sel_df['en_{}'.format(lvl)]/sel_df['genEn']
                    
                    if not doGaussianCore:
                        popt=getMedianAndEffSigma(x,cl)
                    else:
                        rcore_param = getGaussianCoreParameters(x-1,np.linspace(-0.4,0.4,100))
                        if not rcore_param[-1]:
                            print('Discarding fit results at (Et,eta)={:3.2f,:3.2f} for {}'.format(avg_en,avg_eta,ikey))
                        popt=[rcore_param[i] for i in [1,4,2,5]]

                    
                    resols.append([dftitle,eta_cat,en_cat,avg_et,avg_en,avg_eta,lvl]+popt)
                
    resols=pd.DataFrame(resols,columns=['sample','eta_cat','pt_cat','pt','en','eta','level',
                                        'relBias','relBiasUnc','relResol','relResolUnc',
                                       ])
    resols=resols.dropna()
    
    for eta_cat in resols['eta_cat'].unique():        
        sel_resols=resols[resols['eta_cat']==eta_cat]
        eta=sel_resols['eta'].mean()
        etaRange=(sel_resols['eta'].min(),sel_resols['eta'].max())

        if doRatio:
            kwargs={'figsize': (10, 12), 'gridspec_kw': {'height_ratios': (3, 1)},}
            fig, (ax, rax) = plt.subplots(2, 1, sharex=True, **kwargs)
            fig.subplots_adjust(hspace=.07)          
        else:
            fig,ax=plt.subplots(figsize=(8,8))
            
        #colors = ['black','#b35806','#998ec3','#f1a340','#d8daeb','#542788']
        markers=['s','o','v','^']
        resol_popts=[]
        maxy,minyratio,maxyratio=0.,ratioran[0],ratioran[1]
        for ikey,(key_title,df) in enumerate(samples):

            #ci=colors[ikey]
            for ilvl,lvl in enumerate(levels):
                
                marker=markers[ilvl]
                
                mask=(sel_resols['sample']==key_title) 
                mask=mask & (sel_resols['level']==lvl) 
                mask=mask & (sel_resols['relResolUnc']>0) 
                    
                x=sel_resols[mask]['en'].values
                y=sel_resols[mask]['relResol'].values
                yerr=sel_resols[mask]['relResolUnc'].values
                if y.max()>maxy: maxy=y.max()
                
                #ix=1/x
                #y2=y**2
                #y2err=2*y*yerr
                #popt,pcov = curve_fit(lambda x, a,b,c: resol2_func_ix(ix, a,b,c), ix, y2, sigma=y2err,method='dogbox',check_finite=True, bounds=bounds) 
                popt,pcov = curve_fit(lambda x, a,b,c: resol_func(x, a,b,c), x, y, sigma=yerr,method='dogbox',check_finite=True, bounds=bounds) 
                resol_popts.append( popt )
                
                label=key_title
                if not levelsTitle is None:
                    if len(samples)==1: label=levelsTitle[ilvl]
                    else: label += f' ({levelsTitle[ilvl]})'

                fmtargs = {'marker':marker, 'ms':10, 'ls':'None'}
                xlin=np.linspace(np.min(x)*0.9,np.max(x)*1.1,100)
                if nofit:
                    fmtargs['label']=r'%s'%label
                else:
                    plot_fmt_args = {'ls':'-'}
                    if ikey==0: plot_fmt_args['color']='k'
                    g = ax.plot(xlin,resol_func(xlin,*popt),**plot_fmt_args)
                    fmtargs['label']=r'%s : $\frac{%3.2f}{\sqrt{E}}\oplus\frac{%3.2f}{E}\oplus{%3.2f}$'%(label,popt[0],popt[1],popt[2])
                    fmtargs['color'] = g[0].get_color()
                ax.errorbar(x,y,yerr,**fmtargs)
                
                if doRatio:  
                    if ikey==0: continue
                    ref_key=samples[0][0]
                    ref_mask=(sel_resols['sample']==ref_key) & (sel_resols['level']==lvl)
                    merged_df = sel_resols[mask].copy()
                    merged_df=merged_df.merge(sel_resols[ref_mask], 
                                           on="pt_cat",
                                           how='outer',
                                           suffixes=('', '_ref'))
                    merged_df=merged_df.dropna()
                    
                    xratio=merged_df['en'].values
                    yratio=(merged_df['relResol']/merged_df['relResol_ref']).values
                    yratioUnc=(merged_df['relResolUnc']/merged_df['relResol_ref']).values
                    if yratio.max()>maxyratio:
                        maxyratio=yratio.max()
                    if yratio.min()<minyratio:
                        minyratio=yratio.min()
                        
                    #fit the average resolution loss
                    def _pol0(x,a):
                        return np.zeros_like(x)+a
                    avgpopt,avgpcov = curve_fit(_pol0,xratio,yratio,sigma=yratioUnc,method='trf',check_finite=True)

                    #plot                  
                    rax.errorbar(xratio,yratio,yratioUnc,marker=marker,ms=10,ls='None',color=fmtargs['color'],
                                 label=r'$\delta$({})={:3.2f}'.format(label,avgpopt[0]))
                    
                    if not nofit:
                        xlin=np.linspace(np.min(x)*0.9,np.max(x)*1.1,100)
                        rax.plot(xlin, 
                                 resol_func(xlin,*(resol_popts[-1]))/resol_func(xlin,*(resol_popts[0]) ),
                                 ls='--',color=fmtargs['color'],alpha=0.5)
                    rax.plot(xlin, _pol0(xlin,*avgpopt), ls='-',color=fmtargs['color'])
        
        #ax.set_xlim(25,200)
        ax.set_ylim(0,0.4)
        #if maxy>0.4:
        #    ax.set_ylim(0,1.0)
        ax.set_ylabel(r'$\sigma_E~/~E$')
        
        ax.text(1.0,1.02,r'$<|\eta|>={:3.2f}$'.format(eta),transform=ax.transAxes,horizontalalignment='right')
        #ax.text(0.02,0.02,rf'0.5 $\times$ {cl:d}% CI used for resolution',transform=ax.transAxes,horizontalalignment='left')
        ax.text(0.02,0.02,rf'$\sigma_{{eff}}$ used for resolution',transform=ax.transAxes,horizontalalignment='left')
        if not doRatio:
            ax.set_xlabel(r'$E_{gen}$ [GeV]')
        else:
            ncol=3 if len(samples)>2 else 1
            if addLegend: rax.legend(loc='best',ncol=ncol)
            rax.set_xlabel(r'$E_{gen}$ [GeV]')
            rax.set_ylim(*ratioran)
            if maxyratio>ratioran[1] or minyratio<ratioran[0]:
                rax.set_ylim((minyratio*0.8,maxyratio*1.1))
            rax.set_ylabel(r'$(\sigma_E~/~E) / (\sigma_E~/~E)_{ref}$')
            rax.grid()
        ax.grid()

        if addLegend: ax.legend(loc='upper right')
        hep.cms.label(loc=0,data=False,ax=ax,rlabel='') 
        plt.tight_layout()

        if outname:
            plt.savefig('{}_eta{}{}.png'.format(outname,eta_cat,'_gauss' if doGaussianCore else ''))
            plt.close()
        else:
            plt.show()
            
            
def profileXY(x,y,bins,doX=True):

    df = pd.DataFrame({'x' : x , 'y' : y} if doX else {'x' : y , 'y' : x})
    df['bin'] = np.digitize(x, bins=bins)
    df = df.groupby('bin')
    result = df.agg(['mean', 'sem','count'])
    del df
    
    mask=(result[('x','count')]>10)
    result=result[mask]
    
    
    return result[('x','mean')].values,result[('x','sem')].values,result[('y','mean')].values,result[('y','sem')].values
