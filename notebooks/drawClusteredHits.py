import ROOT
import sys
import os
import collections
import numpy as np

_layerz = [ 3221.455, 3231.76, 3252.025, 3262.33, 
            3282.595, 3292.9, 3313.165, 3323.47, 3343.735, 3354.04, 
            3374.305, 3384.61, 3404.875,3415.18, 
            3435.445, 3445.75, 3466.015, 3476.32, 
            3499.835, 3510.14, 3533.655, 3543.96, 
            3567.475, 3577.78, 3601.295, 3611.6,  
            3677.495, 3740.545, 3803.595, 3866.645,
            3929.695, 3992.745, 4055.795, 4118.845,
            4181.895, 4244.945, 4307.995, 4390.245,
            4472.495, 4554.745, 4636.995, 4719.245, 4801.495,
            4883.745, 4965.995, 5048.245, 5130.495, 
          ]

def drawEventsFrom(url,algo,algoTitle,usesMC):

    outdir='events'
    os.system(f'mkdir -vp {outdir}')

    #read events in parallel
    t=ROOT.TChain(f'{algo}/hits')
    t.AddFile(url)
    t.SetAlias('hit_r','sqrt(hit_x*hit_x+hit_y*hit_y)')

    c=ROOT.TCanvas('c','c',600,600)
    c.Draw()
    c.SetFillStyle(0)
    c.SetGridx()
    c.SetGridy()
    c.SetTopMargin(0.01)
    c.SetBottomMargin(0.1)
    c.SetLeftMargin(0.12)
    c.SetRightMargin(0.01)
        
    def _format(clucode=0):
        marker_dict={0:20,1:2,2:70,3:24}
        color_dict={0:ROOT.kGray,1:ROOT.kBlack,2:ROOT.kGreen+3,3:ROOT.kGreen-3}
        t.SetMarkerStyle(marker_dict[clucode])
        t.SetMarkerColor(color_dict[clucode])
    
    for i in range(t.GetEntries()):
    
        t.GetEntry(i)
        if i>50: break
    
        nhits=t.hit_x.size()
    
        for v1,v2 in [('x','y'),('r','z')]:
    
            c.Clear()
            _format(0)
            t.Draw(f'hit_{v1}:hit_{v2}',f'Entry$=={i} && hit_isc==0')
            _format(1)
            t.Draw(f'hit_{v1}:hit_{v2}',f'Entry$=={i} && hit_isc==1','same')
            _format(2)
            t.Draw(f'hit_{v1}:hit_{v2}',f'Entry$=={i} && hit_isc==2','same')
            _format(3)
            t.Draw(f'hit_{v1}:hit_{v2}',f'Entry$=={i} && hit_isc==3','same')
            
            leg=ROOT.TLegend(0.15,0.82,0.4,0.65)
            leg.SetTextFont(42)
            leg.SetBorderSize(0)
            leg.SetFillStyle(0)
            leg.SetTextSize(0.04)
            
            htitles=[]
            t.GetEntry(i)
            isc = [v for v in t.hit_isc]
            isc_counts_dict = collections.Counter(isc)
            for ic in range(4):
                if not ic in isc_counts_dict: continue
                if ic==0: htitles.append('RecHit rejected')
                if ic==1: htitles.append('LC rejected')
                if ic==2: htitles.append('3D Cluster rejected')
                if ic==3: htitles.append('Megacluster' if usesMC else '3D Cluster')
                
            k=0
            for h in c.GetListOfPrimitives():
                if h.GetName()=='TFrame' : continue
                h.GetXaxis().SetTitle(v2 + ' [cm]' if v2!='z' else 'Layer')
                h.GetYaxis().SetTitle(v1 + ' [cm]')
                if h.GetName()!='Graph': continue
                leg.AddEntry(h,f'{htitles[k]} ({h.GetN():d})','p')
                k+=1

            if v1=='r' and usesMC:
                #draw pca trajectory
                gr=ROOT.TGraph()
                gr.SetLineStyle(7)
                gr.SetLineWidth(2)
                gr.SetFillStyle(0)
                gr.SetLineColor(ROOT.kAzure+3)
                for ilay,zlay in enumerate( _layerz ):
                    if t.pca_x[2]<0: zlay *= -1
                    lam=(0.1*zlay-t.pca_x[2])/t.pca_dx[2]
                    xlay=t.pca_x[0]+lam*t.pca_dx[0]
                    ylay=t.pca_x[1]+lam*t.pca_dx[1]
                    rho=np.hypot(xlay,ylay)
                    gr.SetPoint(ilay,ilay+1,rho)
                gr.Draw('l')
                leg.AddEntry(gr,'PCA major','l')

                #mark barycenter
                gr2=ROOT.TGraph()
                gr2.SetMarkerStyle(33)
                gr2.SetMarkerSize(2)
                gr2.SetMarkerColor(46)                
                rhopca=np.hypot(t.pca_x[0],t.pca_x[1])
                dz=np.abs(np.array(_layerz)-10*abs(t.pca_x[2]))
                closestLayers=np.argsort(dz)
                zavg=0.5*(closestLayers[0]+closestLayers[1])+1
                gr2.SetPoint(0,zavg,rhopca)
                gr2.Draw('p')
                leg.AddEntry(gr2,'3D Cluster barycenter','p')

            leg.Draw()
                
            txt=ROOT.TLatex()
            txt.SetNDC()
            txt.SetTextFont(42)
            txt.SetTextSize(0.05)
            txt.DrawLatex(0.15,0.92,algoTitle)
            txt.DrawLatex(0.15,0.87,f'E={t.genergy:3.0f} GeV, |#eta|={t.geta:3.2f}')
            c.Modified()
            c.Update()
            c.SaveAs(f'{outdir}/{algo}_event_{i}_{v1}_{v2}.png')

def main():

    url=sys.argv[1]
    ROOT.gROOT.SetBatch(True)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(0)

    for algo,algoTitle,usesMC in [
            ('ana_lc_ca_r04','CA R=0.4',False),
            ('ana_lc_ca_r02','CA R=0.2',False),
            ('ana_lc_ca_r070_si',"CA' R=0.07cm",False),
            ('ana_lc_ca_r070_si_pca',"CA'+MegaCluster",True),
            ('ana_tk','Clue3D (high)',False),
            ('ana_tk_pca','Clue3D+MegaCluster',True),
            ]:
        drawEventsFrom(url,algo,algoTitle,usesMC)
    
if __name__ == '__main__':
    main()
