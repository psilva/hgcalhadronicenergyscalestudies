#ifndef _HGCDigiTester_h_
#define _HGCDigiTester_h_

#include <tuple>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/ESTransientHandle.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "Geometry/CaloGeometry/interface/CaloGeometry.h"
#include "Geometry/CaloGeometry/interface/CaloSubdetectorGeometry.h"
#include "Geometry/Records/interface/CaloGeometryRecord.h"
#include "DetectorDescription/Core/interface/DDCompactView.h"
#include "SimDataFormats/CaloHit/interface/PCaloHitContainer.h"
#include "SimDataFormats/CaloHit/interface/PCaloHit.h"
#include "SimDataFormats/CaloAnalysis/interface/CaloParticleFwd.h"
#include "SimDataFormats/CaloAnalysis/interface/CaloParticle.h"
#include "DataFormats/ForwardDetId/interface/HGCalDetId.h"
#include "RecoLocalCalo/HGCalRecAlgos/interface/RecHitTools.h"
#include "DataFormats/HGCRecHit/interface/HGCRecHitCollections.h"
#include "DataFormats/CaloRecHit/interface/CaloCluster.h"
#include "DataFormats/HGCalReco/interface/Trackster.h"
#include "UserCode/HGCALHadronicEnergyScaleStudies/interface/SiModuleShape.h"
#include "TNtuple.h"
#include "TTree.h"
#include "TH1F.h"
#include "TVector3.h"

#include "fastjet/ClusterSequence.hh"

/**
   @class HGCRecHitAnalyzer
   @short selects single particle rechits using different algorithms
   - jet algorithm with cone parameter R (eta-phi) or scale invariant coordinates
   - further pruning after a PCA (megacluster like)
*/
class HGCRecHitAnalyzer : public edm::one::EDAnalyzer<edm::one::SharedResources>
{
  
 public:
  
  HGCRecHitAnalyzer( const edm::ParameterSet& );
  
 private:

  void beginJob() override;
  void analyze( const edm::Event&, const edm::EventSetup& ) override;
  void analyzeCaloParticle(const edm::Event &iEvent, const edm::EventSetup &iSetup,const CaloParticle &cp);
  void endJob() override;
  void parseMaskFile(const edm::ParameterSet &iConfig);
  void fillCellGeometry();  
  float computeCglob(const std::vector<float> &hit_en,
                     const std::vector<bool> &is_ceh,
                     float elim_cee=5., float elim_ceh=5., size_t min_nhits=3,
                     float cglob_min=0., float cglob_max=2.);
  void setScaledInvariantCoordinates(fastjet::PseudoJet &j, float r, float z, float zr=1.0); //306.67);
  std::pair<TVector3,TVector3> computeShowerPCA(std::vector<GlobalPoint> &xyz, std::vector<float> &wgt);  
  bool megaClusterAlgo(unsigned int layer, TVector2 &xy, TVector2 &ref);
  std::pair<float,float> getSaturatedEnergy(uint32_t key,float en);
  std::vector<fastjet::PseudoJet> prepareJetOnClusters(const std::vector<reco::CaloCluster> &clusters,std::vector<int> &sellc_idx);
  TVector3 computeShowerPCA(std::vector<GlobalPoint> &xyz, std::vector<int> &wgt);
  
  edm::EDGetTokenT<edm::PCaloHitContainer> simHitsCEE_,simHitsCEH_,simHitsCEHSci_;
  edm::EDGetTokenT<HGCRecHitCollection> recHitsCEE_,recHitsCEH_,recHitsCEHSci_;
  edm::EDGetTokenT<std::vector<reco::CaloCluster>> clusters_token_;
  edm::EDGetTokenT<std::vector<ticl::Trackster> >tracksters_token_;
  edm::EDGetTokenT<CaloParticleCollection> caloParticlesToken_;
  edm::ESGetToken<CaloGeometry, CaloGeometryRecord> caloGeomToken_;

  Int_t event;
  bool crosscalo;
  Float_t e_cee120,e_cee200,e_cee300;
  Float_t e_cehfine120,e_cehfine200,e_cehfine300;
  Float_t e_cehcoarse120,e_cehcoarse200,e_cehcoarse300;
  Float_t e_cehscifine, e_cehscicoarse;
  Float_t e_raw,etruth_raw;
  Float_t c_glob;
  Float_t clusteredEta,clusteredPhi;
  bool pcaCorrected;
  Float_t pcaEta, pcaPhi;
  Int_t rawN,lcN,clusteredN,pcaN;
  Float_t genergy,gpt,geta,gphi,gvradius,gvz;
  std::vector<Float_t> xpca,dxpca;
  std::vector<Float_t> xhit,yhit;
  std::vector<Int_t> chit;
  std::vector<Int_t> zhit;
  TTree *tree_;
  bool dumpGeom_,dumpEvent_,fillCellGeom_;
  std::set<unsigned int> vetoDetIds_;
  TNtuple *cells_;
  std::map<std::string, TH1F *> histos_;

    
  unsigned int hitFilter_,minLCsize_;
  bool useScaledInvariant_,usePCA_;
  int clustJetAlgo_;
  double maxDeltaR_;
  std::map<std::tuple<int,int,int>, SiModuleShape> waferShapes_;

  std::vector<double> weights_, thickness_weights_;

  hgcal::RecHitTools recHitTools_;
};
 

#endif
