#ifndef simoduleshape_h
#define simoduleshape_h

#include <array>

/**
   @short helper struct to hold the vertices of a Si module
 */
class SiModuleShape {

 public:
  
  SiModuleShape() {
    nvert=0;
    vx.fill(0.f);
    vy.fill(0.f);
  }
  
  SiModuleShape(const SiModuleShape&s) {
    nvert=s.nvert;
    vx=s.vx;
    vy=s.vy;
  };
  
  inline bool isIn(float x,float y) {
    /**
       point-in-polygon through path approach
       given modules are convex polygons, do a path starting from the first vertex and
       check if the point in question is always on the same side
       https://www.eecs.umich.edu/courses/eecs380/HANDOUTS/PROJ2/InsidePoly.html (solution 3)
    */

    //if this has been set to 0 nothing can be in
    if(nvert==0) return false;
    
    //loop over vertices
    int curSign(-999);
    for(size_t i=0; i<nvert; i++) {

      size_t i0=i%(nvert+1);
      size_t i1=(i+1)%(nvert);

      //check where the point lies with respect to the vector joining two vertices
      float idelta=(y-vy[i0])*(vx[i1]-vx[i0])-(x-vx[i0])*(vy[i1]-vy[i0]);
      int idelta_sgn=idelta>0 ? 1 : -1;

      //first time assign the sign
      if(i==0) {
        curSign=idelta_sgn;
        continue;
      }

      //first sign flip return with point is not in
      if(curSign*idelta_sgn<0) {
        return false;
      }
    }

    //if the checks didn't find any sign flip the point is contained
    return true;
  }
  
  size_t nvert;
  std::array<float,7> vx,vy;
};


#endif
