# HGCALHadronicEnergyScaleStudies

Analyzer and notebooks for hadron energy scale studies in CMSSW

## Installation 

In lxplus8 the analyzer can be installed as follows:

```
cmsrel CMSSW_14_1_0_pre1
cd CMSSW_14_1_0_pre1/src
cmsenv
git clone https://gitlab.cern.ch/psilva/hgcalhadronicenergyscalestudies.git UserCode/HGCALHadronicEnergyScaleStudies
scram b
```

It is expected to be able to run on RECO outputs containing the usual HGCAL collections.
It produces flat ntuples ready to be inspected for the purpose of studying the energy scale of single particles.
The analyzer can be tested by running locally over a few events

```
cmsRun test/hgcrechitanalyzer_cfg.py  maxEvents=100
```

For larger samples you can use adapt the condor submission and shell script files which can be found under the `test` directory. Then do something like

```
cd test
condor_submit submitRecHits.sub
```

to have the jobs running in condor (1 job/RECO file).

## Training the semi-parametric regression

Before using the notebooks it's useful to merge the chunks of the condor jobs and prepare summary files (pandas format).
This can be done by using the following sequence

```
python notebooks/utils/mergeSamples.py chunks_directory
python notebools/utils/prepareSummaries.py -i csv_file_list -o output_directory
```

For the `prepareSummaries.py` a default is provided if no arguments are passed (K0L eta scan).
In the output directory you'll find a feather file per analysis run in CMSSW with the shower summaries.

The semi-parametric training is better run on a terminal instead of SWAN.
E.g. if using`lxplus-gpu` one should source one of the LCG environments (better to match the one you'll be using in SWAN).

```
source /cvmfs/sft.cern.ch/lcg/views/LCG_106_cuda/x86_64-el9-gcc11-opt/setup.sh
```

And then run the training

```
python notebooks/utils/semiparametric_train_wrapper.py -s totStudies_lc2
```

The scenarios to regress are hardcoded in the `semiparametric_train_wrapper.py` script so please edit those for what you want to do.
After the training a sub-directory called `training` will be created with the `torch` file defining the models and the log files containing the evolution of the loss function for each epoch.

## Analysis

The final plots are made with jupyter notebooks.
The notebooks can be found under the `notebooks` repository.
They are commented and should be self-explanatory (famous last words...).
The notebooks do not need a specific CMSSW installation.
They can be run, e.g. directly in SWAN by clicking on the following icon:

[![SWAN](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/psilva/hgcalhadronicenergyscalestudies.git)

Choose the same LCG environment as the one used for the training.
In both cases use as much mem available as possible (16GB should do it).