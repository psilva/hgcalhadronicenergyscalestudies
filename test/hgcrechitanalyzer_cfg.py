import FWCore.ParameterSet.Config as cms

from Configuration.Eras.Era_Phase2C11I13M9_cff import Phase2C11I13M9
process = cms.Process("ANALYSIS", Phase2C11I13M9)

from FWCore.ParameterSet.VarParsing import VarParsing
options = VarParsing ('standard')
options.register('input', 
                 '/eos/cms/store/group/dpg_hgcal/comm_hgcal/Production/SingleK0LGun_eta1p6to2p9_CMSSW_14_1_0_pre1_D99_vanilla',
                 VarParsing.multiplicity.singleton, 
                 VarParsing.varType.string, 
                 "input directory")
options.register('geometry', 
                 'Extended2026D99', 
                 VarParsing.multiplicity.singleton, 
                 VarParsing.varType.string, 
                 'geometry to use')
options.register('dumpGeometry', 
                 False, 
                 VarParsing.multiplicity.singleton, 
                 VarParsing.varType.bool, 
                 'dump geometry tree')
options.register('dumpEvent', 
                 False, 
                 VarParsing.multiplicity.singleton, 
                 VarParsing.varType.bool, 
                 'dump event tree')
options.register('minLCsize', 
                 2, 
                 VarParsing.multiplicity.singleton, 
                 VarParsing.varType.int, 
                 'min. size of layer cluters to use')
options.parseArguments()


#set geometry/global tag
process.load('Configuration.StandardSequences.Services_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.EventContent.EventContent_cff')
process.load("FWCore.MessageService.MessageLogger_cfi")
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
process.load('Configuration.Geometry.Geometry%sReco_cff'%options.geometry)
process.load('Configuration.Geometry.Geometry%s_cff'%options.geometry)
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase2_realistic', '')

process.MessageLogger.cerr.threshold = ''
process.MessageLogger.cerr.FwkReport.reportEvery = 500

#source/number events to process
import os
if os.path.isdir(options.input):
    fList = ['file:'+os.path.join(options.input,f) for f in os.listdir(options.input) if '.root' in f]
else:
    fList = ['file:'+x if not x.find('/store')==0 else x for x in options.input.split(',')]
print(fList)

process.source = cms.Source("PoolSource",
                            fileNames = cms.untracked.vstring(fList),
                            duplicateCheckMode = cms.untracked.string("noDuplicateCheck")
                        )

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(options.maxEvents) )

from RecoLocalCalo.HGCalRecProducers.HGCalRecHit_cfi import calcWeights,weightsPerLayer_V16
weights=calcWeights(weightsPerLayer_V16)
thickness_weights=[0.75, 0.76, 0.75, 0.85, 0.85, 0.84, 0.69]

process.TFileService = cms.Service("TFileService",
                                   fileName = cms.string(options.output)
                                  )

#analyzer
process.ana = cms.EDAnalyzer("HGCRecHitAnalyzer",
                             dumpGeometry=cms.bool(options.dumpGeometry),
                             dumpEvent=cms.bool(options.dumpEvent),
                             hitFilter=cms.uint32(0),
                             minLCsize=cms.uint32(options.minLCsize),
                             useScaledInvariant=cms.bool(False),
                             maxDeltaR=cms.double(0.4),
                             clustJetAlgo=cms.int32(-1),
                             usePCA=cms.bool(False),
                             weights=cms.vdouble(weights),
                             thickness_weights=cms.vdouble(thickness_weights),
                             maskfile=cms.string("UserCode/HGCALHadronicEnergyScaleStudies/data/mask_geometry.hgcal.txt")
                             )


#process only 1 event if the geometry is to be dumped
#this should be used to check the masks only
if options.dumpGeometry:
    process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(1) )    
    process.ana_no3 = process.ana.clone( maskfile=cms.string("UserCode/HGCALHadronicEnergyScaleStudies/data/mask_geometry.nothrees.hgcal.txt") )
    process.ana_no5 = process.ana.clone( maskfile=cms.string("UserCode/HGCALHadronicEnergyScaleStudies/data/mask_geometry.nofives.hgcal.txt") )
    process.p = cms.Path(process.ana+process.ana_no3+process.ana_no5)

#otherwise process the events with the masks of interest
else:

    def _addROCv3Cscenarios(process,ana_name):
        for i in [1,2,3,4,5,6]:
            setattr(process,f'{ana_name}_{i}', getattr(process,ana_name).clone( hitFilter = cms.uint32(i) ) )
        return process
    
    #CA R=0.4
    process.ana_lc_ca_r04 = process.ana.clone( useSimHits=cms.bool(False),
                                               maskfile=cms.string(""),
                                               clustJetAlgo=cms.int32(1),
                                               maxDeltaR=cms.double(0.4) )
    process = _addROCv3Cscenarios(process,'ana_lc_ca_r04')

    #CA R=0.2
    process.ana_lc_ca_r02 = process.ana_lc_ca_r04.clone( maxDeltaR=cms.double(0.2) )
    
    #modified CA
    process.ana_lc_ca_r070_si = process.ana_lc_ca_r02.clone( useScaledInvariant=cms.bool(True),
                                                             maxDeltaR=cms.double(0.070) )

    #modified CA + megacluster
    process.ana_lc_ca_r070_si_pca = process.ana_lc_ca_r070_si.clone( usePCA=cms.bool(True) )
    process = _addROCv3Cscenarios(process,'ana_lc_ca_r070_si_pca')
    
    
    #TRACKSTER-BASED
    process.ana_tk = process.ana.clone( useSimHits=cms.bool(False),
                                        maskfile=cms.string(""),
                                        maxDeltaR=cms.double(-1) )

    #TRACKSTER-BASED + megacluster
    process.ana_tk_pca = process.ana_tk.clone( usePCA=cms.bool(True) )
    process = _addROCv3Cscenarios(process,'ana_tk_pca')
            
    process.lc_ca = cms.Path(
        process.ana_lc_ca_r04
        +process.ana_lc_ca_r04_1
        +process.ana_lc_ca_r04_2
        +process.ana_lc_ca_r04_3
        +process.ana_lc_ca_r04_4
        +process.ana_lc_ca_r04_5
        +process.ana_lc_ca_r04_6
        +process.ana_lc_ca_r02
    )
    process.lc_ca_si = cms.Path(
        process.ana_lc_ca_r070_si
        +process.ana_lc_ca_r070_si_pca
        +process.ana_lc_ca_r070_si_pca_1
        +process.ana_lc_ca_r070_si_pca_2
        +process.ana_lc_ca_r070_si_pca_3
        +process.ana_lc_ca_r070_si_pca_4
        +process.ana_lc_ca_r070_si_pca_5
        +process.ana_lc_ca_r070_si_pca_6
    )
    process.tk = cms.Path(
        process.ana_tk
        +process.ana_tk_pca
        +process.ana_tk_pca_1
        +process.ana_tk_pca_2
        +process.ana_tk_pca_3
        +process.ana_tk_pca_4
        +process.ana_tk_pca_5
        +process.ana_tk_pca_6
    )
    process.sched = cms.Schedule(
        process.lc_ca,
        process.lc_ca_si,
        process.tk
    )
