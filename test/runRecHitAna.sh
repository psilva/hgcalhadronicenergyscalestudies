#!/bin/bash

inputdir=${1}
outfile=${2}
minlcsize=${3}

cmssw=/afs/cern.ch/user/p/psilva/work/HGCal/ROCv3c/CMSSW_14_1_0_pre1/src
cd $cmssw
eval `scram r -sh`
cd -

cmsRun ${cmssw}/UserCode/HGCALHadronicEnergyScaleStudies/test/hgcrechitanalyzer_cfg.py input=${inputdir} output=${outfile} minLCsize=${minlcsize}
