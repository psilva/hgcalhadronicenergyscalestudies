#include "UserCode/HGCALHadronicEnergyScaleStudies/interface/HGCRecHitAnalyzer.h"
#include "DetectorDescription/OfflineDBLoader/interface/GeometryInfoDump.h"
#include "Geometry/Records/interface/IdealGeometryRecord.h"
#include "Geometry/HGCalCommonData/interface/HGCalGeomRotation.h"
#include "SimDataFormats/CaloTest/interface/HGCalTestNumbering.h"
#include "SimG4CMS/Calo/interface/CaloHitID.h"
#include "DetectorDescription/Core/interface/DDFilter.h"
#include "DetectorDescription/Core/interface/DDFilteredView.h"
#include "DetectorDescription/Core/interface/DDSolid.h"
#include "DataFormats/GeometryVector/interface/Basic3DVector.h"
#include "CLHEP/Units/GlobalSystemOfUnits.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/Exception.h"
#include "FWCore/Utilities/interface/RandomNumberGenerator.h"
#include "FWCore/Utilities/interface/StreamID.h"
#include "CLHEP/Geometry/Point3D.h"
#include "CLHEP/Geometry/Plane3D.h"
#include "CLHEP/Geometry/Vector3D.h"
#include "FWCore/ParameterSet/interface/FileInPath.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "DataFormats/ForwardDetId/interface/HGCScintillatorDetId.h"
#include "DataFormats/ForwardDetId/interface/HGCSiliconDetId.h"
#include "Geometry/HGCalGeometry/interface/HGCalGeometry.h" 
#include "FWCore/ParameterSet/interface/FileInPath.h"
#include "DataFormats/Math/interface/deltaR.h"
#include <fstream>
#include <iostream>
#include <cassert>
#include <math.h>
#include <functional>
#include <numeric>
#include <TVector2.h>
#include <TVector3.h>
#include <Eigen/Core>
#include <Eigen/Dense>

using namespace std;
using namespace fastjet;

typedef math::XYZTLorentzVector LorentzVector;
typedef math::XYZPoint Point;

//
// PLUGIN IMPLEMENTATION
//


//
HGCRecHitAnalyzer::HGCRecHitAnalyzer( const edm::ParameterSet &iConfig ) 
  : simHitsCEE_( consumes<std::vector<PCaloHit>>(edm::InputTag("g4SimHits", "HGCHitsEE")) ),
    simHitsCEH_( consumes<std::vector<PCaloHit>>(edm::InputTag("g4SimHits", "HGCHitsHEfront")) ),
    simHitsCEHSci_( consumes<std::vector<PCaloHit>>(edm::InputTag("g4SimHits", "HGCHitsHEback")) ),
    recHitsCEE_(consumes<HGCRecHitCollection>(edm::InputTag("HGCalRecHit", "HGCEERecHits")) ),
    recHitsCEH_(consumes<HGCRecHitCollection>(edm::InputTag("HGCalRecHit", "HGCHEFRecHits")) ),
    recHitsCEHSci_(consumes<HGCRecHitCollection>(edm::InputTag("HGCalRecHit", "HGCHEBRecHits")) ),
    clusters_token_( consumes<std::vector<reco::CaloCluster> >(edm::InputTag("hgcalMergeLayerClusters",""))),
    tracksters_token_(consumes<std::vector<ticl::Trackster>>(edm::InputTag("ticlTrackstersCLUE3DHigh"))),
    caloParticlesToken_(consumes<CaloParticleCollection>(edm::InputTag("mix","MergedCaloTruth"))),    
    caloGeomToken_(esConsumes<CaloGeometry, CaloGeometryRecord>()),
    dumpGeom_( iConfig.getParameter<bool>("dumpGeometry") ),
    dumpEvent_( iConfig.getParameter<bool>("dumpEvent") ),
    fillCellGeom_(true)
{   
  usesResource(TFileService::kSharedResource);

  hitFilter_ = iConfig.getParameter<unsigned int>("hitFilter");
  minLCsize_ = iConfig.getParameter<unsigned int>("minLCsize");
  usePCA_ = iConfig.getParameter<bool>("usePCA");
  maxDeltaR_ =iConfig.getParameter<double>("maxDeltaR");
  clustJetAlgo_ =iConfig.getParameter<int>("clustJetAlgo");
  weights_ = iConfig.getParameter<std::vector<double> >("weights");
  thickness_weights_ = iConfig.getParameter<std::vector<double> >("thickness_weights");
  
  parseMaskFile(iConfig);
}

//
void HGCRecHitAnalyzer::beginJob() {
  event=0;
  edm::Service<TFileService> fs;
  if(dumpGeom_)
    cells_ = fs->make<TNtuple>("coord","coord","plane:wu:wv:u:v:x:y:z:veto");
  tree_ = fs->make<TTree>("hits","hits");
  tree_->Branch("event",&event,"event/I");
  tree_->Branch("crosscalo",&crosscalo,"crosscalo/O");
  tree_->Branch("e_cee120",&e_cee120,"e_cee120/F");
  tree_->Branch("e_cee200",&e_cee200,"e_cee200/F");
  tree_->Branch("e_cee300",&e_cee300,"e_cee300/F");
  tree_->Branch("e_cehfine120",&e_cehfine120,"e_cehfine120/F");
  tree_->Branch("e_cehfine200",&e_cehfine200,"e_cehfine200/F");
  tree_->Branch("e_cehfine300",&e_cehfine300,"e_cehfine300/F");
  tree_->Branch("e_cehcoarse120",&e_cehcoarse120,"e_cehcoarse120/F");
  tree_->Branch("e_cehcoarse200",&e_cehcoarse200,"e_cehcoarse200/F");
  tree_->Branch("e_cehcoarse300",&e_cehcoarse300,"e_cehcoarse300/F");
  tree_->Branch("e_cehscifine",&e_cehscifine,"e_cehscifine/F");
  tree_->Branch("e_cehscicoarse",&e_cehscicoarse,"e_cehscicoarse/F");
  tree_->Branch("e_raw",&e_raw,"e_raw/F");
  tree_->Branch("etruth_raw",&etruth_raw,"etruth_raw/F");
  tree_->Branch("rawN",&rawN,"rawN/I");
  tree_->Branch("lcN",&lcN,"lcN/I");
  tree_->Branch("clusteredEta",&clusteredEta,"clusteredEta/F");
  tree_->Branch("clusteredPhi",&clusteredPhi,"clusteredPhi/F");
  tree_->Branch("clusteredN",&clusteredN,"clusteredN/I");
  tree_->Branch("pcaCorrected",&pcaCorrected,"pcaCorrected/O");
  tree_->Branch("pcaEta",&pcaEta,"pcaEta/F");
  tree_->Branch("pcaPhi",&pcaPhi,"pcaPhi/F");
  tree_->Branch("pcaN",&pcaN,"pcaN/I");
  tree_->Branch("c_glob",&c_glob,"c_glob/F");
  tree_->Branch("genergy",&genergy,"genergy/F");
  tree_->Branch("gpt",&gpt,"gpt/F");
  tree_->Branch("geta",&geta,"geta/F");
  tree_->Branch("gphi",&gphi,"gphi/F");
  tree_->Branch("gvradius",&gvradius,"gvradius/F");
  tree_->Branch("gvz",&gvz,"gvz/F");
  if(dumpEvent_){
    xpca.resize(3);
    dxpca.resize(3);
    tree_->Branch("pca_x",&xpca);
    tree_->Branch("pca_dx",&dxpca);
    tree_->Branch("hit_x",&xhit);
    tree_->Branch("hit_y",&yhit);
    tree_->Branch("hit_z",&zhit);
    tree_->Branch("hit_isc",&chit);
  }
  
  histos_["noise120"]=fs->make<TH1F>("noise120",";Energy [GeV];Hits",25,0,0.4);
  histos_["noise200"]=fs->make<TH1F>("noise200",";Energy [GeV];Hits",25,0,0.4);
  histos_["noise300"]=fs->make<TH1F>("noise300",";Energy [GeV];Hits",25,0,0.4);
  histos_["noisesci"]=fs->make<TH1F>("noisesci",";Energy [GeV];Hits",25,0,0.4);
}

//
void HGCRecHitAnalyzer::parseMaskFile(const edm::ParameterSet &iConfig) {
  
  //parse the file with the boundaries of the modules to be masked in the analysis
  std::string url=iConfig.getParameter<std::string>("maskfile");
  if(url.size()==0) return;
  
  edm::FileInPath uvmapF(url); 
  std::ifstream inF(uvmapF.fullPath());  
  while(inF) {

    std::string buf;
    getline(inF,buf);

    std::stringstream ss(buf);
    std::vector<std::string> tokens;
    while (ss >> buf)
      if(buf.size()>0)
        tokens.push_back(buf);
    if(tokens.size()<18) continue;
    if(tokens[0]=="plane") continue;  //header

    int ilay(atoi(tokens[0].c_str()));
    int waferU(atoi(tokens[1].c_str()));
    int waferV(atoi(tokens[2].c_str()));
    std::tuple<int,int,int> key(ilay,waferU,waferV);

    SiModuleShape shape;
    shape.nvert = (size_t) atoi(tokens[3].c_str());
    for(size_t i=0; i<7; i++){
      //mm (flat file) -> cm (CMSSW)
      shape.vx[i] = atof(tokens[2*i+4].c_str())/10.; 
      shape.vy[i] = atof(tokens[2*i+5].c_str())/10.;

      //in the original tiling files from Philippe these are not rotated
      if(ilay>=27 && ilay<=33) {
        float dphi(TMath::Pi()/6);
        if(ilay%2==1) dphi *= 0;
        TVector2 pt=TVector2(shape.vx[i],shape.vy[i]).Rotate(dphi);
        shape.vx[i]=pt.X();
        shape.vy[i]=pt.Y();
      }

    }
    waferShapes_[key]=shape;
  }

  std::cout << "Parsed information on " << waferShapes_.size() << " wafers from " << uvmapF.fullPath() << std::endl;
}

//
void HGCRecHitAnalyzer::endJob()
{
}

//
void HGCRecHitAnalyzer::fillCellGeometry() {

  HGCalGeomRotation hgr= {HGCalGeomRotation::SectorType::Sector120Degrees};
  
  //if already filled don't go in the method
  if(!fillCellGeom_) return;

  //loop over CE-E and CE-H Si-only methods
  for(int ig=0; ig<2; ig++){

    const HGCalGeometry*g = 
      static_cast<const HGCalGeometry*>(
        recHitTools_.getGeometry()->getSubdetectorGeometry(
          ig==0 ? DetId::HGCalEE : DetId::HGCalHSi, 
          ForwardSubdetector::ForwardEmpty) );
    
    const std::vector<DetId> &dlist= g->getValidDetIds();
    for(auto key : dlist) {

      HGCSiliconDetId sid(key);        
      unsigned int layer = recHitTools_.getLayerWithOffset(key);     
      GlobalPoint xyz=recHitTools_.getPosition(sid);

      //convert the wafer (u,v) to the first sector (0-120 deg)
      HGCalGeomRotation::WaferCentring wc(HGCalGeomRotation::WaferCentring::WaferCentred);
      if(layer>=34) {
        if(ig==1 && layer%2==1) wc=HGCalGeomRotation::WaferCentring::CornerCentredY;
        if(ig==1 && layer%2==0) wc=HGCalGeomRotation::WaferCentring::CornerCentredMercedes;
      }
      int u0(sid.waferU()),v0(sid.waferV());
      unsigned sector = hgr.uvMappingToSector0(wc,u0,v0);
      std::tuple<int,int,int> mkey(layer,u0,v0);
      int veto=0;
      auto wsit(waferShapes_.find(mkey));
      if(wsit==waferShapes_.end()) continue;

      //flip x depending on the endcap side
      float x(xyz.x()*(-sid.zside()));
      float y(xyz.y());
        
      //rotate back to (0-120 deg) if needed
      if(sector>0){
        float dphi(2.*TMath::Pi()/3.);
        if(sector==1) dphi *= -1;
        TVector2 pt=TVector2(x,y).Rotate(dphi);
        x=pt.X();
        y=pt.Y();
      }
        
      //evaluate if point is in the wafer of reference
      bool isIn(wsit->second.isIn(x,y));
      if(!isIn) {
        vetoDetIds_.insert(key.rawId());
        veto=1;
      }
      
      //save geometry entry in tree if configured to do so
      if(!dumpGeom_) continue;
      cells_->Fill(layer,sid.waferU(),sid.waferV(),sid.cellU(),sid.cellV(),xyz.x(),xyz.y(),xyz.z(),veto);
    }
  }

  std::cout << vetoDetIds_.size() << " DetIds will be vetoed in this analysis" << std::endl;
  
  //no need to repeat this next event
  fillCellGeom_=false;
}


//
void HGCRecHitAnalyzer::analyze( const edm::Event &iEvent, const edm::EventSetup &iSetup)
{
  //read geometry components for HGCAL
  edm::ESHandle<CaloGeometry> geom = iSetup.getHandle(caloGeomToken_);
  recHitTools_.setGeometry(*geom);
  fillCellGeometry();

  //analyze only single particle gun events
  edm::Handle<CaloParticleCollection> caloParticlesHandle;
  iEvent.getByToken(caloParticlesToken_, caloParticlesHandle);
  for(unsigned int i=0; i<caloParticlesHandle->size(); i++) {    

    if(dumpEvent_) {
      xhit.clear();
      yhit.clear();
      zhit.clear();
      chit.clear();
    }
    
    auto const& cp = caloParticlesHandle->at(i);
    analyzeCaloParticle(iEvent,iSetup,cp);
  }  
}


//
void HGCRecHitAnalyzer::analyzeCaloParticle(const edm::Event &iEvent, const edm::EventSetup &iSetup,const CaloParticle &cp) {

  unsigned int lastCEE(recHitTools_.lastLayerEE());
  unsigned int firstCoarse(lastCEE==26? 38 : 39);
    
  //flag if particle interacted before calorimeter
  auto pdgid = cp.pdgId();
  bool crossedBoundary(true);
  if (std::abs(pdgid) != 11) {
    crossedBoundary=cp.g4Tracks()[0].crossedBoundary();
  }
  crosscalo=crossedBoundary;  

  //details on gen particle
  auto genP4=cp.p4();
  //Point genPartVertex=genp->vertex();
  genergy = genP4.energy();
  gpt = genP4.pt();
  geta = genP4.eta();
  gphi = genP4.phi();
  gvradius = 0; //sqrt(pow(genPartVertex.x(),2)+pow(genPartVertex.y(),2));
  gvz = 0; //genPartVertex.z();
  if(fabs(geta)<1.45 || fabs(geta)>3.1) return;

  //SIM-level
  //build list of truth simulated hit identifiers (from SimHits)
  std::set<uint32_t> simHitDetIds;
  edm::Handle<edm::PCaloHitContainer> simHitsCEE,simHitsCEH,simHitsCEHSci;
  iEvent.getByToken(simHitsCEE_,    simHitsCEE);
  iEvent.getByToken(simHitsCEH_,    simHitsCEH);
  iEvent.getByToken(simHitsCEHSci_, simHitsCEHSci);
  for(size_t i=0; i<3; i++) {
    const std::vector<PCaloHit> &simHits((i==0 ? *simHitsCEE : (i==1 ? *simHitsCEH : *simHitsCEHSci)));
    for(size_t j=0; j<simHits.size(); j++) {
        auto sh = simHits[j];
        uint32_t key(sh.id());
        float eta=recHitTools_.getEta(key,gvz);
        if(eta*geta<0) continue;
        simHitDetIds.insert(key);
    }
  }

  //LAYER CLUSTER-level
  edm::Handle<std::vector<reco::CaloCluster>> clusterHandle;
  iEvent.getByToken(clusters_token_, clusterHandle);
  std::set<uint32_t> layerClusteredIds;
  std::vector<int> sellc_idx;
  for(size_t ic=0; ic<clusterHandle->size(); ic++) {
    
    const auto &c = clusterHandle->at(ic);
    
    //HGCAL clusters
    if(c.algo()!=reco::CaloCluster::AlgoId::hgcal_em && c.algo()!=reco::CaloCluster::AlgoId::hgcal_had) continue; 
    
    //at least two hits, some energy and valid position     
    if(c.size()<minLCsize_) continue;
    double en=c.energy();
    if(en<1e-4) continue;
    if(fabs(c.z())<1e-4) continue;
    
    //same endcap as calo particle
    if(c.eta()*geta<0) continue;

    sellc_idx.push_back(ic);
    
    //save detIds of interesting layer clusters
    const auto &hf = c.hitsAndFractions();
    for(auto hfp : hf) {
      layerClusteredIds.insert(hfp.first);
    }
  }

  //3D CLUSTER-level: clustering of layer clusters
  std::vector<int> lc_idx;
  std::set<uint32_t> clus3dDetIds;  
  if(maxDeltaR_>0) {

    //run jet algo and select jet with highest multiplicity
    auto pseudoParticles = prepareJetOnClusters(*clusterHandle,sellc_idx);
    JetDefinition jet_def( (JetAlgorithm)(clustJetAlgo_), maxDeltaR_);
    ClusterSequence cs(pseudoParticles, jet_def);
    auto jets = sorted_by_pt(cs.inclusive_jets());
    size_t ntk=jets.size();
    int idx_sel(-1),maxvtx(0);
    for(size_t i=0; i<ntk; i++) {
      int totalvtx=jets[i].constituents().size();
      if(totalvtx<maxvtx) continue;
      maxvtx=totalvtx;
      idx_sel=i;
    }
    if(idx_sel<0) return;

    //selected jet info
    auto &j = jets[idx_sel];
    clusteredEta=j.eta();
    clusteredPhi=j.phi();
    if(useScaledInvariant_) {
      clusteredEta=TMath::ASinH(1./j.eta());
      clusteredPhi=j.phi()/j.eta();
    }
    for(auto jconst :  j.constituents()) {
      auto idx=jconst.user_index();
      lc_idx.push_back(idx);
    }

  } else {

    edm::Handle<std::vector<ticl::Trackster>> tracksterHandle;
    iEvent.getByToken(tracksters_token_, tracksterHandle);
    
    //select the trackster with the highest multiplicity
    size_t ntk=tracksterHandle->size();
    int idx_sel(-1),maxvtx(0);
    for(size_t i=0; i<ntk; i++) {
      auto tk=tracksterHandle->at(i);
      float teta = tk.barycenter().eta();
      if(teta*geta<0) continue;      
      std::vector<float> &vtx=tk.vertex_multiplicity();
      int totalvtx = (int)std::accumulate(vtx.begin(), vtx.end(), static_cast<float>(0));
      if(totalvtx<maxvtx) continue;
      maxvtx=totalvtx;
      idx_sel=i;
    }
    if(idx_sel<0) return;

    //selected trackster info
    auto &tk = tracksterHandle->at(idx_sel);
    clusteredEta=tk.barycenter().eta();
    clusteredPhi=tk.barycenter().phi();    
    for(auto idx : tk.vertices()) {
      lc_idx.push_back(idx);      
    }
  }

  //save grouped layer clusters
  //compute PCA if required
  TVector3 pcaDir(0,0,0);
  TVector3 pcaBarycenter(0,0,0);
  std::vector<GlobalPoint> pca_xyz;
  std::vector<float> pca_wgts;
  for(auto ic:lc_idx) {
    const auto &c = clusterHandle->at(ic);
    const auto &hf = c.hitsAndFractions();
    for(auto hfp : hf) {
      clus3dDetIds.insert(hfp.first);
    }
    if(!usePCA_) continue;
    pca_xyz.push_back( GlobalPoint(c.x(),c.y(),c.z()) );
    pca_wgts.push_back( (float)c.size() );
  }
  pcaCorrected=false;
  if(usePCA_) {      
    auto pcaResult = computeShowerPCA(pca_xyz,pca_wgts);
    pcaBarycenter = pcaResult.first;
    pcaDir = pcaResult.second;
    //float dEta=pcaDir.Eta()-clusteredEta;
    //float dPhi=TVector2::Phi_mpi_pi(pcaDir.Phi()-clusteredPhi);
    //float dR=sqrt(dEta*dEta+dPhi*dPhi);
    //if(dR>0.1) {
    //  pcaDir=TVector3(cos(clusteredPhi),sin(clusteredPhi),TMath::SinH(clusteredEta));
    //  pcaCorrected=true;
    //}
  } else {
    pcaDir = TVector3(cos(clusteredPhi),sin(clusteredPhi),TMath::SinH(clusteredEta));
  }
  
  //MEGACLUSTER step (if required)
  std::set<uint32_t> pcaClusteredDetIds;
  if(!usePCA_) {
    pcaClusteredDetIds = clus3dDetIds;
    pcaEta=clusteredEta;
    pcaPhi=clusteredPhi;
  }
  else {

    for(auto key : layerClusteredIds) {
      GlobalPoint xyz=recHitTools_.getPosition(key);
      unsigned int layer = recHitTools_.getLayerWithOffset(key);
      TVector2 xy(xyz.x(),xyz.y());

      float lambda=(xyz.z()-pcaBarycenter.Z())/pcaDir.z();
      TVector2 xy_ref(pcaBarycenter.X()+lambda*pcaDir.X(),
                      pcaBarycenter.Y()+lambda*pcaDir.Y());

      //if pointing to (0,0)
      //float rho=xyz.z()/TMath::SinH(pcaDir.Eta());                                                
      //TVector2 xy_ref(rho*cos(pcaDir.Phi()),rho*sin(pcaDir.Phi()));

      if(!megaClusterAlgo(layer,xy,xy_ref)) continue;
      pcaClusteredDetIds.insert(key);
    }

    pcaEta=pcaDir.Eta();
    pcaPhi=pcaDir.Phi();
  }

  
  //increment event as this is going to be stored
  event++;
  
  //raw and layer clustered counts
  rawN=simHitDetIds.size();
  lcN=layerClusteredIds.size();
  clusteredN=clus3dDetIds.size();
  pcaN=pcaClusteredDetIds.size();  

  //std::cout << event << " cfg=(" << maxDeltaR_ << "," << usePCA_ << ") "
  //          << " gen=(" << geta << "," << gphi << ") #raw=" << rawN << " #lcN=" << lcN
  //          << " clus=(" << clusteredEta << "," << clusteredPhi << ") #=" << clusteredN
  //          << " pca=(" << pcaDir.Eta() << "," << pcaDir.Phi() << ") #=" << pcaN << std::endl;
  
  //now read rec hits and build the sums
  const auto& cee_hits = iEvent.get(recHitsCEE_);
  const auto& ceh_hits = iEvent.get(recHitsCEH_);
  const auto& cehsci_hits = iEvent.get(recHitsCEHSci_);
  
  //save for an event display
  if(dumpEvent_) {    
    xpca[0]=pcaBarycenter.X(); xpca[1]=pcaBarycenter.Y(); xpca[2]=pcaBarycenter.Z();
    dxpca[0]=pcaDir.X();       dxpca[1]=pcaDir.Y();       dxpca[2]=pcaDir.Z();
    for(auto key : simHitDetIds) {
      unsigned int layer = recHitTools_.getLayerWithOffset(key);     
      GlobalPoint xyz=recHitTools_.getPosition(key);
      xhit.push_back(xyz.x());
      yhit.push_back(xyz.y());
      zhit.push_back(layer);
      int clusCode(0);
      if(layerClusteredIds.find(key) != layerClusteredIds.end() ) {
        clusCode=1;
        if(!usePCA_) {
          if(pcaClusteredDetIds.find(key) != pcaClusteredDetIds.end()) {
            clusCode=3;
          }
        }
        else {
          if(clus3dDetIds.find(key) != clus3dDetIds.end() )
            clusCode=2;
          if(pcaClusteredDetIds.find(key) != pcaClusteredDetIds.end())
            clusCode=3;
        }
      }
      chit.push_back(clusCode);
    }
  }

  //finally store sums of energies for selected hits
  std::vector<float> sien;
  std::vector<bool> siisceh;
  e_cee120=0; e_cee200=0; e_cee300=0;
  e_cehfine120=0; e_cehfine200=0; e_cehfine300=0;
  e_cehcoarse120=0; e_cehcoarse200=0; e_cehcoarse300=0;
  e_cehscifine=0; e_cehscicoarse=0;
  e_raw=0; etruth_raw=0;  
  for(size_t i=0; i<3; i++) {
    
    const auto & hits(i==0 ? cee_hits : (i==1 ? ceh_hits : cehsci_hits) );

    for(auto d : hits) {
      
      uint32_t key( d.id().rawId() );
      bool isSci = recHitTools_.isScintillator(key);
      int thick = isSci ? -1 : recHitTools_.getSiThickIndex(key);
      float en = d.energy();
      std::pair<float,float> enmip = getSaturatedEnergy(key,en);
      en=enmip.first;
      float nmips=enmip.second;
      
      if(simHitDetIds.find(key)!=simHitDetIds.end()) {
        etruth_raw += en;        
      } else {
        if(isSci)    histos_["noisesci"]->Fill(en);
        else{
          if(thick==0) histos_["noise120"]->Fill(en);
          if(thick==1) histos_["noise200"]->Fill(en);
          if(thick==2) histos_["noise300"]->Fill(en);
        }
      }

      //from this point forward use only hits which are clustered by final algo
      if(pcaClusteredDetIds.find(key)==pcaClusteredDetIds.end()) continue;

      //filter also low energy hits (noisy)
      if(en<0.05) continue;
      
      unsigned int layer = recHitTools_.getLayerWithOffset(key);
      bool isEE(layer<=lastCEE);
      
      //apply DetId-based filters
      if(!isSci) {
        if(vetoDetIds_.find(key)!=vetoDetIds_.end()) continue;
      }

      //total reconstructed energy
      e_raw += en;

      //sub-sums
      if(isEE) {
        if(thick==0)      e_cee120+= en; 
        else if(thick==1) e_cee200+= en; 
        else              e_cee300+= en; 
      }
      else {
        bool isFine(layer<firstCoarse);
        if(!isSci) {
          if(isFine) {
            if(thick==0)      e_cehfine120+= en; 
            else if(thick==1) e_cehfine200+= en; 
            else              e_cehfine300+= en; 
          }else {
            if(thick==0)      e_cehcoarse120+= en; 
            else if(thick==1) e_cehcoarse200+= en; 
            else              e_cehcoarse300+= en; 
          }
        }
        else{
          if(isFine) e_cehscifine += en;
          else       e_cehscicoarse += en;
        }
      }

      //si hits for c_glob computation
      if(!isSci) {
        sien.push_back(nmips);
        siisceh.push_back(!isEE);
      }

    } //end hit loops
  } // end hit collections

  c_glob=computeCglob(sien,siisceh);
  
  tree_->Fill();
}



/**
    @short computes the global compensation variable for a collection of hits
    elim - the threshold to be used
    min_hits - minimum number of hits
    cglob_{min,max} - limit output to these values
*/
float HGCRecHitAnalyzer::computeCglob(const std::vector<float> &hit_en,
                                      const std::vector<bool> &is_ceh,
                                      float elim_cee, float elim_ceh, size_t min_nhits,
                                      float cglob_min, float cglob_max)
{
  size_t nhits(hit_en.size());
  if(nhits<min_nhits) return -1.;
    
  float mean_en = TMath::Mean(hit_en.begin(),hit_en.end());

  //count hits below average or below elim~5MIP
  size_t nbelow_elim(0);
  size_t nbelow_mean(0);
  for(size_t i=0; i<nhits; i++) {
    nbelow_mean += (hit_en[i]<mean_en);
    float elim(is_ceh[i]?elim_ceh:elim_cee);
    nbelow_elim += (hit_en[i]<elim);
  }
  if(nbelow_mean==0) return -1.;

  //cglob ratio
  float cglob=float(nbelow_elim)/float(nbelow_mean);
  cglob=min(cglob_max,max(cglob_min,cglob));
    
  return cglob;
}

//
std::pair<TVector3,TVector3> HGCRecHitAnalyzer::computeShowerPCA(std::vector<GlobalPoint> &xyz, std::vector<float> &wgt) {
  
  //first compute the barycenter
  float sumWgts(0.f);
  std::vector<float> xyz0(3,0.f);
  for(size_t i=0; i<xyz.size(); ++i){
    sumWgts+=wgt[i];
    xyz0[0]+=wgt[i]*xyz[i].x();
    xyz0[1]+=wgt[i]*xyz[i].y();
    xyz0[2]+=wgt[i]*xyz[i].z();
  }
      
  //now fill the covariance matrix
  Eigen::Vector3d point;
  point << 0., 0., 0.;
  Eigen::Vector3d barycenter;
  barycenter << xyz0[0]/sumWgts, xyz0[1]/sumWgts, xyz0[2]/sumWgts;
  Eigen::Vector3d sigmas;
  sigmas << 0., 0., 0.;
  Eigen::Vector3d sigmasEigen;
  sigmasEigen << 0., 0., 0.;
  Eigen::Matrix3d covM = Eigen::Matrix3d::Zero();
  for(size_t i=0; i<xyz.size(); i++){
    float w = wgt[i];
    point[0] = xyz[i].x();
    point[1] = xyz[i].y();
    point[2] = xyz[i].z();
    for (size_t ix = 0; ix < 3; ++ix)
      for (size_t iy = 0; iy <= ix; ++iy) {
        covM(ix, iy) += w * (point[ix] - barycenter[ix]) * (point[iy] - barycenter[iy]);
        covM(iy, ix) = covM(ix, iy);
      }
  }
  covM *= 1. / (sumWgts-1);
  
  // Perform the actual decomposition
  // as eigen stores the eigenvalues in ascending order the desired eigenvector is in the last column
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigensolver(covM);
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d>::EigenvectorsType eigenvectors_fromEigen = \
    eigensolver.info() != Eigen::Success ? eigenvectors_fromEigen.Zero() : eigensolver.eigenvectors();  
  size_t icol(2); 
  TVector3 pcaDir(eigenvectors_fromEigen(0,icol),eigenvectors_fromEigen(1,icol),eigenvectors_fromEigen(2,icol));
  if (pcaDir.Z() * xyz0[2] < 0) pcaDir *= -1.;

  //return also the barycenter
  TVector3 pcaBarycenter(barycenter[0],barycenter[1],barycenter[2]);

  return std::pair<TVector3,TVector3>(pcaBarycenter,pcaDir);
}



//
void HGCRecHitAnalyzer::setScaledInvariantCoordinates(fastjet::PseudoJet &j, float r, float z, float zr) {    
  float pt=j.pt();
  float eta=(r/z)*zr;
  float phi=eta*j.phi();
  j.reset_PtYPhiM(pt,eta,phi); 
}

//
std::vector<PseudoJet> HGCRecHitAnalyzer::prepareJetOnClusters(const std::vector<reco::CaloCluster> &clusters,std::vector<int> &sellc_idx) {

  //fastjet clustered layer clusters
  //read the layer clusters and build the pseudoparticles to cluster
  std::vector<PseudoJet> pseudoParticles;
  for(auto ic : sellc_idx) {
    const auto &c = clusters[ic];
    //save as pseudo-jet for the clustering
    TVector3 evec=TVector3(c.x(),c.y(),c.z()).Unit();
    double en=c.energy();
    PseudoJet ip=PseudoJet(en*evec.X(),en*evec.Y(),en*evec.Z(),en);
    if(useScaledInvariant_) {
      float r=sqrt(pow(c.x(),2)+pow(c.y(),2));
      setScaledInvariantCoordinates(ip,r,c.z());  
    }
    ip.set_user_index(ic);
    pseudoParticles.push_back( ip );
  }

  return pseudoParticles;
}

//
bool HGCRecHitAnalyzer::megaClusterAlgo(unsigned int layer, TVector2 &xy, TVector2 &ref) {

  //configure parameters depending on detector region
  float RM=3.5;
  float rmin(RM),rmax(2*RM);
  float lmin(1.),lmax(26.);
  if(layer>=26) { rmin=2.0*RM; rmax=4.0*RM; lmin=27.; lmax=37.;}
  if(layer>=38) { rmin=4.0*RM; rmax=6.0*RM; lmin=38.; lmax=47.;}

  //interpolate max alowed radius for this layer
  float a=(rmax-rmin)/(lmax-lmin);
  float b=rmin-a*lmin;
  float r=a*layer+b;

  //return if it should be accepted or not
  return ((xy-ref).Mod()<r);
}

//
std::pair<float,float> HGCRecHitAnalyzer::getSaturatedEnergy(uint32_t key,float en){

  unsigned int layer = recHitTools_.getLayerWithOffset(key);
  bool isSci = recHitTools_.isScintillator(key);
  int thick = isSci ? -1 : recHitTools_.getSiThickIndex(key);
  unsigned int lastCEE(recHitTools_.lastLayerEE());
  bool isEE(layer<=lastCEE);
  
  //Erec = weight * nmips / thick_weight <=> nmips = Erec * thick_weight / weight
  //weights are in keV, energy in GeV
  float mipSF(1./(weights_[layer]*1e-3));
  if(!isSci) {
    size_t idx(isEE ? 0 : 3);
    if(thick==0) idx+=0;
    else if(thick==1) idx+=1;
    else idx+=2;
    mipSF *= thickness_weights_[idx];
  }
  else{
    mipSF *= thickness_weights_[6];
  }

  //hit saturation : assume 95% of the ADC range is usable
  float maxmips(1.e9);
  float nmips(en*mipSF);
  float fsc(160.*0.95);
  float mipeq(1.28);
  if(hitFilter_==5 && thick==0) fsc=80.*0.95;
  if(thick==1) {fsc=160.*0.95; mipeq=2.24; }
  if(thick==2) {fsc=320.*0.95; mipeq=3.504; }

  //construction scenario
  if(hitFilter_==1 || hitFilter_==5 || hitFilter_==6) {
    if(isSci) {
      if(layer>=37) {
        maxmips=18.;
        if(hitFilter_==6) maxmips=9.;
      }
    }else{
      if(layer==25 || layer==26 || layer>=37) maxmips = fsc/mipeq;        
    }
  }

  //construction scenario but no saturation on SiPM-on-tile
  if(hitFilter_==2 && !isSci) {
    if(layer==25 || layer==26 || layer>=37) maxmips = fsc/mipeq;        
  }
  
  //construction but all CE-H mix is affected
  if(hitFilter_==3) {
    if(isSci) {
      if(layer>=34) maxmips=18.;
    }else{
      if(layer==25 || layer==26 || layer>=34) maxmips = fsc/mipeq;        
    }
  }

  //construction but all CE-H mix is affected and no saturation on SiPM-on-tile  
  if(hitFilter_==4 && !isSci) {
    if(layer==25 || layer==26 || layer>=34) maxmips = fsc/mipeq;        
  }
  
  //re-assign energy to the hit  
  nmips=min(nmips,maxmips);
  en=nmips/mipSF;

  return std::pair<float,float>(en,nmips);
}
 
//define this as a plug-in
#include "FWCore/Framework/interface/MakerMacros.h"
DEFINE_FWK_MODULE(HGCRecHitAnalyzer);
